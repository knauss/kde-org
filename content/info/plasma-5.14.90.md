---
version: "5.14.90"
title: "KDE Plasma 5.14.90, Beta Release"
errata:
    link: https://community.kde.org/Plasma/5.15_Errata
    name: 5.15 Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
unstable: true
---

This is a Beta release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the [Plasma 5.14.90 announcement](/announcements/plasma-5.14.90).
