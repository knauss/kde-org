<ul>

<!-- ARCHLINUX -->
<li><a href="http://www.archlinux.org/">Archlinux</a>
    <ul type="disc">
      <li>
         3.5 KDE testing branch: <tt><a href="ftp://xentac.net/tpowa">Archlinux KDE 3.5 Testing Branch</a></tt>
      </li>
    </ul>
  <p />
</li>

<!-- KUBUNTU -->
<li><a href="http://www.kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         Hoary and Breezy (Intel i386): <tt><a href="http://www.kubuntu.org/announcements/kde-35beta1.php">Kubuntu KDE 3.5 Beta 1 packages page</a></tt>
      </li>
    </ul>
  <p />
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.novell.com/linux/suse/">SUSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/README">README</a>)
      :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      10.0:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/ix86/10.0/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/x86_64/10.0/">AMD x86-64</a>
    </li>
    <li>
      9.3:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/ix86/9.3/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/x86_64/9.3/">AMD x86-64</a>
    </li>
    <li>
      9.2:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/ix86/9.2/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/x86_64/9.2/">AMD x86-64</a>
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
  </ul>
  <p />
</li>

</ul>
