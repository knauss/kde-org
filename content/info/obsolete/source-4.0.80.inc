<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdeaccessibility-4.0.80.tar.bz2">kdeaccessibility-4.0.80</a></td><td align="right">6,1MB</td><td><tt>9e35fd8d008280abd18b5a50493d4580</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdeadmin-4.0.80.tar.bz2">kdeadmin-4.0.80</a></td><td align="right">1,9MB</td><td><tt>2326f9f7c4c64e00c5c733bc6fab7b97</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdeartwork-4.0.80.tar.bz2">kdeartwork-4.0.80</a></td><td align="right">41MB</td><td><tt>5d55359e3f47d2693a0b0117c57a6dac</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdebase-4.0.80.tar.bz2">kdebase-4.0.80</a></td><td align="right">4,2MB</td><td><tt>c520fd296accc864646bb6bc22bce9f0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdebase-runtime-4.0.80.tar.bz2">kdebase-runtime-4.0.80</a></td><td align="right">48MB</td><td><tt>53d6e7be3787c755895fd9ad73283e04</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdebase-workspace-4.0.80.tar.bz2">kdebase-workspace-4.0.80</a></td><td align="right">29MB</td><td><tt>01a05d8830a942a54f8c92f353a24174</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdebindings-4.0.80.tar.bz2">kdebindings-4.0.80</a></td><td align="right">4,0MB</td><td><tt>2773ce3ef97cb9b07d37aef463dddb9e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdeedu-4.0.80.tar.bz2">kdeedu-4.0.80</a></td><td align="right">54MB</td><td><tt>cd5f8e53d3a3bba591ec3c0df6c081a6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdegames-4.0.80.tar.bz2">kdegames-4.0.80</a></td><td align="right">29MB</td><td><tt>f6c7978a87f9cade87d4bbbc30e89ca6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdegraphics-4.0.80.tar.bz2">kdegraphics-4.0.80</a></td><td align="right">2,4MB</td><td><tt>02a036a5172afbbd2ce4cee42a27ec40</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdelibs-4.0.80.tar.bz2">kdelibs-4.0.80</a></td><td align="right">9,0MB</td><td><tt>40aec4dc4167d3df23beb71b470181cd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdemultimedia-4.0.80.tar.bz2">kdemultimedia-4.0.80</a></td><td align="right">1,5MB</td><td><tt>ae6c15d224b961c6cee213ba0945e7e7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdenetwork-4.0.80.tar.bz2">kdenetwork-4.0.80</a></td><td align="right">7,1MB</td><td><tt>98622509ee073fc0c2623dbf9fe74692</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdepim-4.0.80.tar.bz2">kdepim-4.0.80</a></td><td align="right">13MB</td><td><tt>e08922f0620dcc7f35719406c6997c6b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdepimlibs-4.0.80.tar.bz2">kdepimlibs-4.0.80</a></td><td align="right">1,8MB</td><td><tt>5fee4da26fbb77b3a668e2e5b5b76bea</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdesdk-4.0.80.tar.bz2">kdesdk-4.0.80</a></td><td align="right">4,7MB</td><td><tt>bc5aeb23c631198a8d893930d6aa555b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdetoys-4.0.80.tar.bz2">kdetoys-4.0.80</a></td><td align="right">1,3MB</td><td><tt>57475817d88f04431dc4254b459a6933</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdeutils-4.0.80.tar.bz2">kdeutils-4.0.80</a></td><td align="right">2,0MB</td><td><tt>e03dcae95c56fc12d3643ee4f4e272e6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.80/src/kdewebdev-4.0.80.tar.bz2">kdewebdev-4.0.80</a></td><td align="right">2,5MB</td><td><tt>4b94096189a2f6089f3ac14111a14b25</tt></td></tr>
</table>
