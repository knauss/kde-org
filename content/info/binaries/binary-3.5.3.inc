<ul>

<!-- ARCH LINUX -->
<li><a href="http://www.archlinux.org/">Arch Linux</a>
  :
  <ul type="disc">
    <li>Packages: <a href="ftp://ftp.archlinux.org/testing/os/i686">ftp://ftp.archlinux.org/testing/os/i686</a></li>
    <li>
      To install: pacman -S kde
    </li>
  </ul>
  <p />
</li>

<!-- KUBUNTU -->
<li><a href="http://www.kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         Kubuntu 6.06 LTS (dapper): <a href="http://kubuntu.org/announcements/kde-353.php">Intel i386, AMD64 and 
PowerPC</a>
      </li>
    </ul>
  <p />
</li>

<!-- Pardus 
<li>
 <a href="http://www.pardus.org.tr/">Pardus</a>
    <ul type="disc">
      <li>
         1.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/Pardus/">Intel i386</a>
      </li>
    </ul>
  <p />
</li>
-->

<!-- kde-redhat 
<li><a href="http://kde-redhat.sourceforge.net/">KDE RedHat (unofficial) Packages</a>:
(<a href="http://apt.kde-redhat.org/apt/kde-redhat/kde-org.txt">README</a>)
<ul type="disc">

 <li>All distributions: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/all/">(noarch,SRPMS)</a></li>

 <li>Red Hat 7.3: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/7.3/i386/">(i386)</a></li>

 <li>Red Hat 9: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/9/i386/">(i386)</a> </li>

 <li>Red Hat Enterprise Linux 3: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/3/i386/">(i386)</a> </li>

 <li>Red Hat Enterprise Linux 4:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/kde.repo">(kde.repo)</a>,
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/4/i386/">(i386)</a>
 	<!-- not available (yet) -->
 	<!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/4/x86_64/">Red Hat Enterprise 4 (x86_64)</a>-->
 </li>

 <li>Fedora Core 3:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
 	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/3/i386/">(i386)</a>
	<!-- not available (yet) -->
	<!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/3/x86_64/">(x64_64)</a> -->
 </li>

 <li>Fedora Core 4:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/i386/">(i386)</a>
	<!-- not available (yet) -->
 	<!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/x86_64/">(x64_64)</a> -->
 </li>
 <li>Fedora Core 5:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
        <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/5/i386/">(i386)</a>
        <!-- not available (yet) -->
        <!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/x86_64/">(x64_64)</a> -->
 </li>

</ul>
 <p />
</li>

<!-- SLACKWARE LINUX  -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.3/contrib/Slackware/10.2/README">README</a>)
   :
   <ul type="disc">
     <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.3/contrib/Slackware/noarch/">Language packages</a>
     </li>
     <li>
        10.2: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.3/contrib/Slackware/10.2/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!-- SUSE LINUX -->
<li>
  <a href="http://www.opensuse.org">SuSE Linux</a>
  The SUSE KDE packages will be publicly developed in the openSUSE project from 
now on. This means that the packages are no longer available via the 
supplementarry tree from ftp.suse.com, but via the <a 
href="http://en.opensuse.org/Build_Service">Build Service</a> repositories 
from openSUSE. Please read <a 
href="http://en.opensuse.org/Build_Service/User">this page</a> for 
installation instructions. 
  <ul type="disc">
    <li>
        <a 
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.1/">SUSE 
Linux 10.1 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a 
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.0/">SUSE 
Linux 10.0 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a 
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_9.3/">SUSE 
Linux 9.3 YUM repository</a> (64bit and 32bit)
    </li>
  </ul>
  <p /> 
</li>


</ul>
