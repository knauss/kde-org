KDE Project Security Advisory
=============================

Title:          Okular: Local binary execution via action links
Risk Rating:    Low
CVE:            CVE-2020-9359
Versions:       Okular < 1.10.0 (tarball name okular-20.04.0.tar.xz)
Date:           12th March 2019


Overview
========
Okular can be tricked into executing local binaries via specially crafted
PDF files.

This binary execution can require almost no user interaction.

No parameters can be passed to those local binaries.

We have not been able to identify any binary that will cause actual damage,
be it in the hardware or software level, when run without parameters.

We remain relatively confident that for this issue to do any actual damage,
it has to run a binary specially crafted. That binary must have been deployed
to the user system via another method, be it the user downloading it directly
as an email attachment, webpage download, etc. or by the system being already
compromised.


Solution
========
- Update to Okular >= 1.10.0
- or apply the following patch:
https://invent.kde.org/kde/okular/-/commit/6a93a033b4f9248b3cd4d04689b8391df754e244


Workaround
==========
There's no real workaround other than not opening PDF files from untrusted sources.


Credits
=======
Thanks to Mickael Karatekin from Sysdream Labs for the discovery and to
Albert Astals Cid for the fix.
