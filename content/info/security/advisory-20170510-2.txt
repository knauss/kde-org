KDE Project Security Advisory
=============================

Title:          smb4k: unauthorized local command execution as root
Risk Rating:    High
CVE:            CVE-2017-8849
Versions:       smb4k <= 2.0.0
Date:           10 May 2017


Overview
========
Smb4k contains a logic flaw in which mount helper binary
does not properly verify the mount command it is being asked to run.

This allows calling any other binary as root since the
mount helper is typically installed as suid.

Solution
========
Update to smb4k 2.0.1 (when released)

Or apply the following patches:
smb4k 2.0.0: https://commits.kde.org/smb4k/a90289b0962663bc1d247bbbd31b9e65b2ca000e
smb4k 1.2.3: https://commits.kde.org/smb4k/71554140bdaede27b95dbe4c9b5a028a83c83cce

Credits
=======
Thanks to Sebastian Krahmer from SUSE for the report and
to Albert Astals Cid and Alexander Reinholdt from KDE for the fix.
