KDE Project Security Advisory
=============================

Title:          Fix kwalletd CBC encryption handling
Risk Rating:    Low
CVE:            CVE-2013-7252
Platforms:      All
Versions:       kwalletd < Applications 14.12.1, KF5::KWallet < 5.6.0
Author:         Valentin Rusu <kde@rusu.info>
Date:           9 January 2015

Overview
========

Until KDE Applications 14.12.0, kwalletd incorrectly handled CBC encryption blocks when
encrypting secrets in kwl files. The secrets were still encrypted, but the
result binary data corresponded to an ECB encrypted block instead of CBC.

Impact
======

The ECB encryption algorithm, even if it'll scramble user data, it'll produce
same encrypted byte sequence for the same input text. As a result, attackers
may eventually find-out the encrypted text.

Solution
========

For kde-runtime KWallet upgrade to KDE Applications 14.12.1 or apply the following patch:
  http://quickgit.kde.org/?p=kde-runtime.git&a=commit&h=14a8232d0b5b1bc5e0ad922292c6b5a1c501165c

For KDE Frameworks 5 KWallet upgrade to 5.6.0 or apply the following patch:
  http://quickgit.kde.org/?p=kwallet.git&a=commit&h=6e588d795e6631c3c9d84d85fd3884a159b45849

Credits
=======

Thanks to Itay Duvdevani for finding the issue and for letting us know.
Thanks to Valentin Rusu for implementing the fix.
