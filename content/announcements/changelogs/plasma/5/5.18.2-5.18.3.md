---
aliases:
- /announcements/plasma-5.18.2-5.18.3-changelog
hidden: true
plasma: true
title: Plasma 5.18.3 Complete Changelog
type: fulllog
version: 5.18.3
---

### <a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/bluedevil/e0e0c50a92d15e5d27ec66f5db7103ebe67bf2f7'>Commit.</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Flatpak: make sure we don't issue new queries after cancelling. <a href='https://commits.kde.org/discover/d55a38c935355c3b3e6ee8d6a1bad078113094d5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404622'>#404622</a>
- Flatpak: prevent crash. <a href='https://commits.kde.org/discover/6af2c5b1fe40494a55423e9da3210bf816ef90de'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418308'>#418308</a>
- Pk: only refetch update details once. <a href='https://commits.kde.org/discover/451605085006a18a7bf6503e9f84c52ec2545194'>Commit.</a>
- Pk: only notify about state changes when state changes. <a href='https://commits.kde.org/discover/b9cbec73b23350974b2578660ebc68b9f1eb3a91'>Commit.</a>
- Port away from own weird Qt class. <a href='https://commits.kde.org/discover/1ea61ddbb84917bb3941ef5e2dfe2d2796051421'>Commit.</a>
- FeaturedModel: fix how we deal with errors. <a href='https://commits.kde.org/discover/24958506b7be5cd9cde237b6097402059d28dc16'>Commit.</a>
- Remove unneeded declarations. <a href='https://commits.kde.org/discover/380b2689f7bc28bad484365603d2c6a38399a205'>Commit.</a>
- Simplify Discover load. <a href='https://commits.kde.org/discover/664f2f76a01b577c634a89bae6a53d66278a672f'>Commit.</a>
- Make sure we don't crash. <a href='https://commits.kde.org/discover/1b0992a5375f2243d1c8fdb2ac5efdb991d619bd'>Commit.</a>
- Flatpak: build with older libflatpaks. <a href='https://commits.kde.org/discover/5cae00d1dbd94a584c9c63f7ff7fb5f893b228e4'>Commit.</a>
- Flatpak: include more information on the transaction error message. <a href='https://commits.kde.org/discover/f6426073bc09757395dd3325a0185a7a9c5767fd'>Commit.</a> See bug <a href='https://bugs.kde.org/417078'>#417078</a>
- Flatpak: forgot to commit this method. <a href='https://commits.kde.org/discover/da6e56f8d0ef5787d00a7ccde42da2b15697bff5'>Commit.</a>
- Flatpak: show the right icon for the source. <a href='https://commits.kde.org/discover/6ad0f3224844ea31351015ea4a085396d28cd57d'>Commit.</a>
- Reviews: expose the reviewed package version on the delegate. <a href='https://commits.kde.org/discover/d58668698f5fd7556e9227fedd3318c22ecb868a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417675'>#417675</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27554'>D27554</a>
- Flatpak: simplify code. <a href='https://commits.kde.org/discover/2b90e06c69630be4abb55aed85ba83e204418c8c'>Commit.</a>
- Polish the "removing this repo will uninstall these apps" message. <a href='https://commits.kde.org/discover/7c6405dc8bc585d615db686733d6774cf202b008'>Commit.</a>
- Flatpak: improve the list of resources to remove toghether with the source. <a href='https://commits.kde.org/discover/db3cbccd15351671ed73482499119512aabb2527'>Commit.</a>
- Improve how we report errors in removing sources. <a href='https://commits.kde.org/discover/25c0d5f7f3076c7742247ed070cd17e25ff254bd'>Commit.</a>
- Flatpak: make it easier to remove a remote in use. <a href='https://commits.kde.org/discover/0937ac6e335b10cfa5f09c2d91fe3453a33a96cd'>Commit.</a>
- Port QProcess::startDetached deprecated method. <a href='https://commits.kde.org/discover/d41ee4d88ba9df82522d939c072e663261547496'>Commit.</a>
- Pk: simplify initialisation. <a href='https://commits.kde.org/discover/6f7f3c266658dad7579e2f6afcca51bba16fd39f'>Commit.</a>
- Make status label on Updates page consistent. <a href='https://commits.kde.org/discover/41eefd8c799ce613abc9a61ae2e893301061ec0e'>Commit.</a>
- Emit properties only when they change. <a href='https://commits.kde.org/discover/a48a3d7de72c12acda0d7706cf59e7b04570f8df'>Commit.</a>
- Remove deprecated constructs. <a href='https://commits.kde.org/discover/22535a90b7137efa6a4b1381cd62395ae4f27c31'>Commit.</a>
- Port Application Page's form layout to Kirigami FormLayout. <a href='https://commits.kde.org/discover/40742348cf052310bb74fceaec33f906ee0e4f26'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27183'>D27183</a>
- Reply->error() is deprecated in qt5.15. <a href='https://commits.kde.org/discover/ea0a8ac949767db0b551023c5cafaee01f6b3768'>Commit.</a>
- Change notification button text to "View Updates". <a href='https://commits.kde.org/discover/7126e11498ac6948ff3dca3f7b2c70b39de436de'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D26889'>D26889</a>
- Hopefully fix neon builds. <a href='https://commits.kde.org/discover/fe3a5c7c77908be1307692c9327f04adf1cd2275'>Commit.</a>
- Flatpaknotifier: only refresh the right installation when it gets modified. <a href='https://commits.kde.org/discover/68aa2b1fd4922de6bdb3abed35b7eac7d8124337'>Commit.</a>

### <a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a>

- Fix windows compilation, QString.sprintf() is deprecated. <a href='https://commits.kde.org/drkonqi/9489b933e5bc76fc7bb4ac9dc072a06d387e14d0'>Commit.</a>
- Focus email input line by default. <a href='https://commits.kde.org/drkonqi/ed33c2803511c2f9d6cb5d125b4be79821e903dd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418309'>#418309</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27776'>D27776</a>

### <a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a>

- Set correct version. <a href='https://commits.kde.org/kactivitymanagerd/e15056dfaa30f1fb5ece8acc94ddf206754809d5'>Commit.</a>
- KF5 only deprecate up to target frameworks. <a href='https://commits.kde.org/kactivitymanagerd/c59d40946df8aaa6a42c66746b50864ebf11a4ea'>Commit.</a>

### <a name='kdecoration' href='https://commits.kde.org/kdecoration'>KDE Window Decoration Library</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/kdecoration/3edddf86bc523078080988a1387b2aa2a35b023a'>Commit.</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [applets/notes] Fix drag&drop links and cursor shape regression. <a href='https://commits.kde.org/kdeplasma-addons/b03539a0d8d3b9e5a7c580c926c618bde0dbe374'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417953'>#417953</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27673'>D27673</a>

### <a name='kgamma5' href='https://commits.kde.org/kgamma5'>Gamma Monitor Calibration Tool</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/kgamma5/19e34eb93250f2fe81700a137a0898218356d518'>Commit.</a>

### <a name='kmenuedit' href='https://commits.kde.org/kmenuedit'>KMenuEdit</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/kmenuedit/0d273e5d9efd44b602195ed7b8de0920c3e5df33'>Commit.</a>

### <a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/ksshaskpass/24e38756f73cae6e74faf8ead96122ffb369027a'>Commit.</a>

### <a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a>

- Linux/cpuinfo.c: grow buffer size as needed for 12+ core CPUs. <a href='https://commits.kde.org/ksysguard/031bd7603e39dad609a597c64ff66fbec418d999'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384515'>#384515</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27362'>D27362</a>
- Only deprecate up to target frameworks. <a href='https://commits.kde.org/ksysguard/8418095b5f308e62d2c2acd492a74c1f0c205a5c'>Commit.</a>

### <a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/kwallet-pam/bdd699d7998208340975ddfbd6a96ee2b4a8ee59'>Commit.</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Notify about decorations button order change. <a href='https://commits.kde.org/kwin/76c3174b019fe000f273f68df039088721f709e2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27904'>D27904</a>

### <a name='kwrited' href='https://commits.kde.org/kwrited'>kwrited</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/kwrited/51c44e343f20da3eea2773b66cd89366c9541332'>Commit.</a>

### <a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a>

- Fix: handle when backend fails to load/initialize. <a href='https://commits.kde.org/libkscreen/ff98585ea5541012b68604e34b7fec383a487cd9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27625'>D27625</a>

### <a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/libksysguard/fcb50353aa33b14e61f119ff8dd663597ed2bdcb'>Commit.</a>
- Only link to Qt5WebChannel if Qt5WebEngineWidgets available. <a href='https://commits.kde.org/libksysguard/ba7f78716af618db5556fc17e421397fe67e96af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27347'>D27347</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [applets/taskmanager] Show PA-related features even when audio indicators are disabled. <a href='https://commits.kde.org/plasma-desktop/3b2e4ddaa01ad067924f4efe0380bac1445232fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418164'>#418164</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27684'>D27684</a>
- [emojier] Set minimum window height and width. <a href='https://commits.kde.org/plasma-desktop/d0247bf5695413f8de83e5c48e4b05874d077752'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418195'>#418195</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27679'>D27679</a>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/plasma-integration/15812fb26dbd99343b3ddbbce031248ef6ec1282'>Commit.</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- [kded] Remove html tags from password dialog. <a href='https://commits.kde.org/plasma-nm/afc230d55362d3be52ac2d96701dfda3f604a635'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418625'>#418625</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- [KCM]Fix content below scrollbars. <a href='https://commits.kde.org/plasma-pa/552b0384ffd205a148df62239058cee9a4a6702e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416331'>#416331</a>. Fixes bug <a href='https://bugs.kde.org/417447'>#417447</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27577'>D27577</a>
- [KCM]Set implicitWidth for main page. <a href='https://commits.kde.org/plasma-pa/8dea974eacf09855a8ba2a0a87b1b5e9f90becb4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27648'>D27648</a>

### <a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/plasma-vault/131245f5c2c4b36a619fef462da07596cd51ce7f'>Commit.</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [gmenu-dbusmenu-proxy] Fix radio button state detection. <a href='https://commits.kde.org/plasma-workspace/ea358c896668a5c74ed8c405358c919e94adc8d7'>Commit.</a> See bug <a href='https://bugs.kde.org/418385'>#418385</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27885'>D27885</a>
- [gmenu-dbusmenu-proxy] Pass action "target" in invocation. <a href='https://commits.kde.org/plasma-workspace/503bf4f3a54267e22592c3e0ee246c20a2485a3d'>Commit.</a> See bug <a href='https://bugs.kde.org/418385'>#418385</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27884'>D27884</a>
- Degrade qCInfo to qCDebug. <a href='https://commits.kde.org/plasma-workspace/9a4f259c48205bf1137c829a899a1a6153bad01f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27879'>D27879</a>
- Demote jump list actions to PossibleMatches. <a href='https://commits.kde.org/plasma-workspace/18422ededcc9ec8f3e0b80e75228fe7b4a487829'>Commit.</a> See bug <a href='https://bugs.kde.org/418529'>#418529</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27886'>D27886</a>
- ItemContainer: disconnect signals in destructor. <a href='https://commits.kde.org/plasma-workspace/8f0da90f18ffa69efdc485f137096f36aedb3909'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417603'>#417603</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27650'>D27650</a>

### <a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/plymouth-kcm/0d2e9d064d25585c3ebb8ecccfc0dc9d9740aeae'>Commit.</a>

### <a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/polkit-kde-agent-1/6b3a6ebaff63ae2e1dc1597a18edcd56759e2ed8'>Commit.</a>

### <a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/sddm-kcm/49b5511cab6bebcddb091e2257975cb9b15f2bfe'>Commit.</a>
- Align wallpaper dialog button to bottom. <a href='https://commits.kde.org/sddm-kcm/541d8adc5f14b8b5ab694d184ea0a8d82cfce6a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418173'>#418173</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27667'>D27667</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Fix connection to invalid signal. <a href='https://commits.kde.org/systemsettings/ead61e26ab7843f47ca932b8d22696b35125ad04'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27686'>D27686</a>

### <a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a>

- Only deprecate up to target frameworks. <a href='https://commits.kde.org/user-manager/61ad7f492297defa6a28d8b674979b25228e4bbd'>Commit.</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Use content_type as fallback in appchooser dialog. <a href='https://commits.kde.org/xdg-desktop-portal-kde/58af54b91de9fe4ab383c31c94ccd136cd7d6f06'>Commit.</a>
- Only deprecate up to target frameworks. <a href='https://commits.kde.org/xdg-desktop-portal-kde/c21d6340a8401b9046e236dde18ec71863438604'>Commit.</a>
- FileChooser: do not return empty file list when no local file is selected. <a href='https://commits.kde.org/xdg-desktop-portal-kde/4d4ff532be8b94634e3878bf2011a082a17bbe9b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418155'>#418155</a>