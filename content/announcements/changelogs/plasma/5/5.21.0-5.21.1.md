---
aliases:
- /announcements/plasma-5.21.0-5.21.1-changelog
title: Plasma 5.21.1 complete changelog
version: 5.21.1
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Revert "Revert "Do not draw background or line in toolbars if the color scheme has no headers group"". [Commit.](http://commits.kde.org/breeze/3685a1673e117e8c3b3c0539f6271ede3dfe00c2) Fixes bug [#433118](https://bugs.kde.org/433118)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Correct usage of preferredHeight. [Commit.](http://commits.kde.org/discover/de8a162c7de597d5f102ebd1135d26ea680eb777) 
+ Use more appropriate delete and download icons in compact app delegate. [Commit.](http://commits.kde.org/discover/f86d58a4d5aa94f8a8152076022471333e75d121) 
+ Remove weird padding at the bottom of resource lists. [Commit.](http://commits.kde.org/discover/7993cdf853b6b46ae8d4a6afb58f0e7971ffc7f2) 
+ Pk: make sure search queries eventually finish. [Commit.](http://commits.kde.org/discover/2b28baa68cc1d2c942be9247b0f9fb8d0d1ab80b) Fixes bug [#432384](https://bugs.kde.org/432384)
+ Properly show the pointing hand on screenshot thumbnails. [Commit.](http://commits.kde.org/discover/13f3aadca186269c50a531519c89a939de2fefa6) 
+ Show the correct screenshot when clicking it. [Commit.](http://commits.kde.org/discover/357d51ff9902c3019da9cc16ccb59a63d1e304be) Fixes bug [#433123](https://bugs.kde.org/433123)
+ Pk: Improve updates progress when doing an offline update. [Commit.](http://commits.kde.org/discover/c730100fd5bb69f7ef38ef5658ce3948725d7c66) 
+ Don't truncate reviews in compact mode. [Commit.](http://commits.kde.org/discover/b4d039ebfc29488cd05c301999f9ddaca9640c5b) Fixes bug [#433078](https://bugs.kde.org/433078)
+ Notifier: Reduce the unattended updates idle timeout to 15'. [Commit.](http://commits.kde.org/discover/83af8c78dc56f9bca60059145752cbb5128d0cbb) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/discover/fe6349cade2c3a00283b8cf44e8aa4fded242646) 
+ Increase maximum width of review dialog to a sane size. [Commit.](http://commits.kde.org/discover/4e79743b6fc982d9472653e0123e2e2902d983dc) Fixes bug [#432807](https://bugs.kde.org/432807)
{{< /details >}}

{{< details title="KDE Hotkeys" href="https://commits.kde.org/khotkeys" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/khotkeys/546784259d6a5b23749a0217e6584a781644b340) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/kinfocenter/8bdfabcf21a969676f008cee7809546f88d744c5) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ [kded] Fix selecting OSD action by keyboard. [Commit.](http://commits.kde.org/kscreen/ccad4537e03b1249b912c51fa3c9c4b4f82e5319) Fixes bug [#432481](https://bugs.kde.org/432481)
{{< /details >}}

{{< details title="KSysGuard" href="https://commits.kde.org/ksysguard" >}}
+ Force device statistics refresh rate to be always one second. [Commit.](http://commits.kde.org/ksysguard/16c5713dabb1c843d4ad81ba84195b1bbe796712) Fixes bug [#433063](https://bugs.kde.org/433063)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ [platforms/drm] accept both keypress and keyrelease event in filter. [Commit.](http://commits.kde.org/kwin/1ad65f3f34f886973e889718144c2c3d4ce63f9f) 
+ Fix build. [Commit.](http://commits.kde.org/kwin/2f4a744621f382fb5f42b810c36c7a67880b5b22) 
+ [platforms/drm] only enable output on key press not release. [Commit.](http://commits.kde.org/kwin/b5940e35554e92396d89678cb86ca18fc51f97cb) 
+ Create Xcursor sprites with correct format. [Commit.](http://commits.kde.org/kwin/b43c25650932feb58b9268190a05e441fb6fe444) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/kwin/ca50c016a15f09a3a3b96f3d657003512ee2db48) 
+ Fix crash on pasting too soon after copying from XWayland. [Commit.](http://commits.kde.org/kwin/27c01b9b0d79c072c0a53ab7e1e1b601b18b31a8) 
+ Effects/wobblywindows: Allow model geometry and real geometry get out of sync. [Commit.](http://commits.kde.org/kwin/705e4fcc688852fec50c08559b6b58e2df2c0973) Fixes bug [#433187](https://bugs.kde.org/433187)
+ Effects/wobblywindows: Refactor the update loop. [Commit.](http://commits.kde.org/kwin/31bfff47b8187d00ab65235e9fdae02d974d6a37) 
+ X11: Introduce an envvar to force software vsync. [Commit.](http://commits.kde.org/kwin/ea932230d94c72bd24b364bce5ccb4944a94342c) Fixes bug [#433094](https://bugs.kde.org/433094)
+ X11: Introduce an envvar to prevent sync'ing to vblanks. [Commit.](http://commits.kde.org/kwin/70965e96ca459f18bbf08b3879ce599a68fa6ac5) See bug [#433094](https://bugs.kde.org/433094)
+ Properly clean up DrmGpu. [Commit.](http://commits.kde.org/kwin/d3c3d8c2f640b5a3be3c3bbb1bfbb689266e1e3b) See bug [#433145](https://bugs.kde.org/433145)
+ Inputmethod: honour SNI disabled state. [Commit.](http://commits.kde.org/kwin/72d3f30e0bacd0ed6c5a56ed37990dde5f2b7a24) 
+ Sprinkle static keywords. [Commit.](http://commits.kde.org/kwin/5f04af5664644bab844aedfbcf29c9e06a741139) 
+ X11: Properly detect whether swap events have to disabled. [Commit.](http://commits.kde.org/kwin/47b67a887ab9c29d665efafea25be0c1994fdbd5) 
+ Inputmethod: toggle the inputmethod if we get second show request. [Commit.](http://commits.kde.org/kwin/c9a87b4cf8f135d3d0daff36e9d0ce22a20f4630) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Guard against null configloader in destructor. [Commit.](http://commits.kde.org/libksysguard/b3fd4f2f3a4904d4c64168bff8b427c221d7fab1) Fixes bug [#433431](https://bugs.kde.org/433431)
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/libksysguard/54babff83135eb26a09c54145e9cee38dd593e58) 
+ Add method CGroupDataModel::isAvailable. [Commit.](http://commits.kde.org/libksysguard/76a3570ab2f9fab98aa6ccc9ceafbbc29323db06) 
+ Don't emit dataChanged for invalid indices. [Commit.](http://commits.kde.org/libksysguard/7c46d893a14e04a17cae446dd1b71643c76a72bf) See bug [#431155](https://bugs.kde.org/431155)
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ Fix launching empty query. [Commit.](http://commits.kde.org/milou/592f01cfa93335775931c6a6e13a8a0517ce00d8) 
{{< /details >}}

{{< details title="Plasma Browser Integration" href="https://commits.kde.org/plasma-browser-integration" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-browser-integration/d8d6395a1cb3f5aaf544107f60f05abd84f86b61) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [applets/kickoff] Always capitalize section header letters. [Commit.](http://commits.kde.org/plasma-desktop/6ddaa4cebda77a33041a96bfa4301d4a1ec6ac90) Fixes bug [#433217](https://bugs.kde.org/433217)
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-desktop/11acc15801bacd395b9b8c77f63bc28adb4d8cd2) 
+ Launch runners KCM in systemsettings. [Commit.](http://commits.kde.org/plasma-desktop/fbce7c7fe105f342d205a025ad1f27a4514eaa87) Fixes bug [#433101](https://bugs.kde.org/433101)
+ Kcms/keyboard: fix migration. [Commit.](http://commits.kde.org/plasma-desktop/64546c81d43f8b10487d9c6c5bb0fab4de457d13) Fixes bug [#431923](https://bugs.kde.org/431923)
+ [Kickoff] Remove redundant hover filter. [Commit.](http://commits.kde.org/plasma-desktop/e86a68a5cb168d896393d6548b22ceacb839c82e) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-desktop/55a0934b47eac18fae91106decd32738d0472141) 
{{< /details >}}

{{< details title="Plasma Nano" href="https://commits.kde.org/plasma-nano" >}}
+ Use plasma components for the widget explorer. [Commit.](http://commits.kde.org/plasma-nano/f883d8b07e7bdd34636ddc862afbd1f34036c5ae) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-nm/3f531d4daa4b98e2f35cf5efaf2ea5b8cda9c027) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-nm/842413382f514a320a536de82ba9b93c72734687) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Unref stream after unsetting callbacks. [Commit.](http://commits.kde.org/plasma-pa/02c07a571651aaa22683dc9b6f08aee5ded8403b) See bug [#432482](https://bugs.kde.org/432482)
{{< /details >}}

{{< details title="Plasma Phone Components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Tweak app drawer behavior. [Commit.](http://commits.kde.org/plasma-phone-components/7bcbaff17725aebbdfea6f67a381d96771584769) 
+ Enable the drag handler only when the containment has focus. [Commit.](http://commits.kde.org/plasma-phone-components/171f35eb19e16e00cc2378ed318b35bc60c2f229) 
+ Close sliding panel as soon as it's off the screen for no delay. [Commit.](http://commits.kde.org/plasma-phone-components/cf020e0fe6dab16c285fc719b42e8f6cea0f9071) 
+ Cancel sliding panel animations on touch so it's more responsive. [Commit.](http://commits.kde.org/plasma-phone-components/ad21b85b0e851832998bd03f104ff9579638c8fb) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-phone-components/3e2cef7336e27bc0c6a0de9ccaff31a73c867c09) 
+ Respect max favorite count when dropping. [Commit.](http://commits.kde.org/plasma-phone-components/7cc94efcb8e44ebb0c61eb69d723fae4eef17561) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Fix duplicate name columns appearing when configuring columns. [Commit.](http://commits.kde.org/plasma-systemmonitor/3e1140460ef092c325106a57432b14bde2a054e1) Fixes bug [#433216](https://bugs.kde.org/433216)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Fix case in logout applet config key. [Commit.](http://commits.kde.org/plasma-workspace/d9a32341623bc63e27b8cac2702bd03c39841dbd) Fixes bug [#433320](https://bugs.kde.org/433320)
+ Calculatorrunner: Fix mixed hex+decimal calculations. [Commit.](http://commits.kde.org/plasma-workspace/a3b017ccb56be918163fed350e181ec4fd57db73) Fixes bug [#431362](https://bugs.kde.org/431362)
+ Fix font installation. [Commit.](http://commits.kde.org/plasma-workspace/c8395e7afad7c63ee5cceee72faa57399c0f7704) 
+ Locations runner: Fix absolute filepath + arguments. [Commit.](http://commits.kde.org/plasma-workspace/6315e05e282f06b2f79ca5d966aa55df2aa6877e) Fixes bug [#433053](https://bugs.kde.org/433053)
+ Enforce SESSION_MANAGER is exported before plasmashell is started. [Commit.](http://commits.kde.org/plasma-workspace/399cf129bfb5bf511bb71f684e1d2536a3ea2a9a) 
+ Libtaskmanager: Allow to launch executables. [Commit.](http://commits.kde.org/plasma-workspace/849cb67fcf8ff928074e5a0719efcbd90b11a75d) Fixes bug [#433148](https://bugs.kde.org/433148)
+ Move ksmserver's ksplash notifying to ksmserver. [Commit.](http://commits.kde.org/plasma-workspace/a3f5ac1296e93eefaca1f70d48c6c03c688f3946) Fixes bug [#432364](https://bugs.kde.org/432364)
+ Handle closeSession being called concurrently. [Commit.](http://commits.kde.org/plasma-workspace/e4cbac8543edbb4209adea1e56c43777b1a46d56) 
+ [libkworkspace] Interim fix for the logout issue. [Commit.](http://commits.kde.org/plasma-workspace/74fef0a9973e62df16ff8fc97a795bce1fa2a273) Fixes bug [#432460](https://bugs.kde.org/432460)
+ Fixup! [keyboard applet] fix TypeError garbage in log. [Commit.](http://commits.kde.org/plasma-workspace/b05cb534c4a5e196104511caad1aeded5aad6581) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-workspace/9df495e02f026bafed0d1bf8e7bb36a64e9d70c1) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-workspace/9b9e80a5ac4f14762f22d11435445b35f099af96) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Core: when we get request to wakeup turn dpms on. [Commit.](http://commits.kde.org/powerdevil/d1bf7bfca40dc619255f0c89b980b7e5107938dd) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/powerdevil/404ee90d093c441ec82ade152a9a0a59eb5db25d) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/powerdevil/36aad22cf8a58f7bd2817e2aeb978a70e380840d) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ [RangeSlider] Remove inset, endVisualPosition and palette. [Commit.](http://commits.kde.org/qqc2-breeze-style/ca8d5262c9ce17b7b9ce2c8970a6a4d43294c498) 
+ Use Templates.RangeSlider to not crash. [Commit.](http://commits.kde.org/qqc2-breeze-style/40830295a5ec288332dfda62d8360b60170719d1) 
+ [Slider] Remove endVisualPosition. [Commit.](http://commits.kde.org/qqc2-breeze-style/cb602961531495db3a4fca57f4b1c7d204efb7cd) 
+ [SliderGroove] Rework sizing. [Commit.](http://commits.kde.org/qqc2-breeze-style/97d539a6e7609daa37be0b095c79dcb4c86db811) 
+ [SliderHandle] Adjust x/y animation velocity and color animation. [Commit.](http://commits.kde.org/qqc2-breeze-style/cc4b62b74bf703b2d319daa96b42d5e5c351b58b) 
+ [Slider] Remove inset. [Commit.](http://commits.kde.org/qqc2-breeze-style/42a6d616878d6e3ce3daa63176b62c3d691602c2) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Allow for easier syncing of Plasma font. [Commit.](http://commits.kde.org/sddm-kcm/56449c24560e87a9a61c242b59483b057d22917f) Phabricator Code review [D23257](https://phabricator.kde.org/D23257). Fixes bug [#432930](https://bugs.kde.org/432930)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ [sidebarmode] Fix header height for QWidget KCMs. [Commit.](http://commits.kde.org/systemsettings/48fecba07b5ec361a5c2be8d8892adcb93901e0e) See bug [#433028](https://bugs.kde.org/433028)
+ [sidebar mode] Fix back button icons. [Commit.](http://commits.kde.org/systemsettings/14b5ff1e99735b94ec83a6e2888216c4f7f65367) Fixes bug [#433062](https://bugs.kde.org/433062)
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/systemsettings/d95645cd820b9490eeed4cb730dfef1a06e95b9d) 
{{< /details >}}

