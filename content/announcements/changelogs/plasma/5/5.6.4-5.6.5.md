---
aliases:
- /announcements/plasma-5.6.4-5.6.5-changelog
hidden: true
plasma: true
title: Plasma 5.6.5 Complete Changelog
type: fulllog
version: 5.6.5
---

### <a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a>

- Don't let the delegate overflow the view. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1b5ce678c3dc7094d883a0b2b3bbd612207acce8'>Commit.</a>
- Make sure we don't show in the carousel elements that aren't available. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=de0ad09b453bd6bc119f039f3b28c780569b1994'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363029'>#363029</a>
- Restore the initial page after loading is over. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=70b052a38ff416887364cb2f03bd0a69ab5c33e1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363432'>#363432</a>
- When packagekit reports a message, print it on the console. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ee2deacbf9695290981cb6b16bfda53e911af811'>Commit.</a>
- Include the actual error message in the error messagebox. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f71dedce35bef2bc990d8b3fabdd5c05497689b2'>Commit.</a>
- Every time we stop fetching, sync m_packages and m_updatingPackages. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=8bbbea5e4356487c9cb8349c13580942ca9ef6c2'>Commit.</a>
- Fetch updates after un/installing packages. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=96b20849c754bf524946dd2c2b4405d762b446da'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362818'>#362818</a>

### <a name='kde-cli-tools' href='http://quickgit.kde.org/?p=kde-cli-tools.git'>kde-cli-tools</a>

- Create ~/.local/share/mime/packages/ if it doesn't exist. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=c2aa2a46d51793d26dc6e93e60b5933cb1193e56'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356237'>#356237</a>. Code review <a href='https://git.reviewboard.kde.org/r/128055'>#128055</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- Remove pointless layout. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=25c2075a97b401754298a31560e2207527dccc03'>Commit.</a>
- Shows a trace when the url value is wrong. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e175e95f4db87a608c1535110bf6210b7b7d2813'>Commit.</a>
- Don't consider idle every time the window is hidden. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=4abe7d041c2e067c7d76efc065a4646de293601f'>Commit.</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- [autotests/wayland] Try to make PointerInputTest::testMouseActionActiveWindow more robust. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ee785db147f7763b36c99bb79487f3672aea382b'>Commit.</a>
- Destroy decoration when DecorationBridge is destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=54d770d5ca22f4ed75084d2bfb63395b6d83cd63'>Commit.</a>
- [decorations] Don't recreate Renderer once the Compositor is destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=65d707d24cf8da094096808402f600cc0d372379'>Commit.</a>
- Destroy ShellClient when the SurfaceInterface gets destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=dc8c33e856be8bcdec4e9423c60a85931ec100d2'>Commit.</a>
- Be sure isCurrentTab returns true. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c3cd8df7954f4b136deb67a763103ce333bc0468'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127985'>#127985</a>
- Delay maximize button click to next event cycle. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6cd0d5a54acf618d094097b6222f331352abe7b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362772'>#362772</a>

### <a name='milou' href='http://quickgit.kde.org/?p=milou.git'>Milou</a>

- [Milou] Take label height into account for delegate height. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=bd14a260e651a1f017014958f31f735e926e5e2c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352696'>#352696</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- [Panel Configuration] Disallow pressing Escape whilst manipulating panel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ad1313fe277fe6ccd9702fb267f1e10e753c78c9'>Commit.</a>
- [Kickoff] Use a single MouseArea for opening user manager. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=feac04468de1af30881b64c09e961424241e21ab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363528'>#363528</a>
- [Panel Configuration] Close when pressing Escape. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f32e0c990e99b62b6f535e0ee299ff5b81c1c83c'>Commit.</a>
- [Desktop Toolbox] Close toolbox popup before triggering action. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e3bf0a5bb0a973a104c52ab3c22a8928105cad9f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363161'>#363161</a>
- [taskmanager plugin] Dismiss context menu when associated window is closed. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=62b272fc7837f92ad3b0d05d65c033ec27a175dc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128030'>#128030</a>. See bug <a href='https://bugs.kde.org/362713'>#362713</a>
- [taskmanager plugin] Prevent from dereferencing dangling pointer. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8a61f9614f54bffdc1a44bdcffd6f9ccb3586102'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128021'>#128021</a>
- Fix opening recent docs on newer KF5. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8a9772d8673a58583317b4906a9352d6bf44a8e2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363337'>#363337</a>
- Fix building on Linux with clang/libc++. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d8197688122e6b3787283ef59fa5ddd3c518662b'>Commit.</a>
- [taskmanager] Fixing typos. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d44faae67cf724987e7838494c5d3b01c4a0db57'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127753'>#127753</a>
- Cleanup and fixup KConfig handling for componentchooser. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=27ebf75bd44101976d8392eec4ff4d20f495fb69'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127918'>#127918</a>
- Clear error string on refresh. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=99e278d7635986f3aa3cea19d98526803689b962'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363112'>#363112</a>
- Fix icon never turning visible when transitioning from startup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b55b14a00785e454ab5f0bc80a1bb8da4ecfd944'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362957'>#362957</a>

### <a name='plasma-integration' href='http://quickgit.kde.org/?p=plasma-integration.git'>plasma-integration</a>

- Set KFileDialog overwrite option for saving to follow the Qt option. <a href='http://quickgit.kde.org/?p=plasma-integration.git&amp;a=commit&amp;h=31ead90605238dfc67c3dac0eb9fd9fd7d072051'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127944'>#127944</a>. Fixes bug <a href='https://bugs.kde.org/360666'>#360666</a>

### <a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a>

- Applet: disable dynamic sorting while password dialog is displayed. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=948f39f34ccffe5f6dedfc2c9393026715a78f4a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362700'>#362700</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- Force focus to the panel on AcceptingInputStatus. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9ffcb3178cea8b06fdf964fd05cf28319d1ffc8b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364276'>#364276</a>
- Fix installation of the notification applet library. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=42b64e1bd2c64a43f47a1f9d0b8895e3f3fcea9d'>Commit.</a>
- [Icon Widget] Don't use plasma theme for icon. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b434a0f05ed93e40c50c78b31f16fd7afafe1ff3'>Commit.</a>
- [PanelView] Fix auto hide. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e1ae057fef297b46d17256d497e8e2f4e24c1264'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362105'>#362105</a>
- [KSplashQML] Don't wait for KWin to start on Wayland. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9cec9ed2a87b4cba7a1092267456974f86aaf546'>Commit.</a>
- Battery icon no longer errorneously reports an empty battery if computer has none. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a19fcfaf90db8ebc6e704917448ccfde7ae0ae59'>Commit.</a> See bug <a href='https://bugs.kde.org/362924'>#362924</a>
- Don't read empty icons from config-stored launcher URL. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3613733a933f4010f8b570f4aa6a1f5cb4dbe5ba'>Commit.</a> See bug <a href='https://bugs.kde.org/362957'>#362957</a>