---
aliases:
- /announcements/plasma-5.13.2-5.13.3-changelog
hidden: true
plasma: true
title: Plasma 5.13.3 Complete Changelog
type: fulllog
version: 5.13.3
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- PK: Make sure we resolve the transaction application if it fails. <a href='https://commits.kde.org/discover/d4ed6e5ed987050d59b2b006b2cac6c765905b9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395994'>#395994</a>
- KNS: don't start searches before initializing. <a href='https://commits.kde.org/discover/11a05813b6e558f52459f98a4d1d15280c9ee842'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395966'>#395966</a>
- Report a progressing change when we finish to set up and there's nothing pending. <a href='https://commits.kde.org/discover/26dee5442da203f3f73b5292c50100bd21b76fae'>Commit.</a>
- Recheck fetching after we remove a backend. <a href='https://commits.kde.org/discover/a42b130357bcd26ce8a16f5231cdd2552cd93371'>Commit.</a>
- Flatpak: don't crash if we get null updates. <a href='https://commits.kde.org/discover/99c67ded1cf1f86ddd8f13a96a93c9168e351029'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394713'>#394713</a>
- Don't set property twice. <a href='https://commits.kde.org/discover/dc667b4aeaf21df90bbb4a495dc300e4590d9d18'>Commit.</a>
- Trigger a refresh when appstream data isn't available when on aptcc. <a href='https://commits.kde.org/discover/7bae1c831f0cb63a643949eab626da38cc7105f8'>Commit.</a>
- Don't catch unneeded value in lambda. <a href='https://commits.kde.org/discover/c0b69142bbe01861b07a6d736af114913dbc51c2'>Commit.</a>
- Fix warning: use the right API to get the component desktop id. <a href='https://commits.kde.org/discover/9ce57430962027d76f9622990afccdbf21f0520e'>Commit.</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Don't have two methods called the same in parent and children class without relation. <a href='https://commits.kde.org/kde-gtk-config/5271dd80ea99415924337296c9b8348af26738ea'>Commit.</a>
- --warning. <a href='https://commits.kde.org/kde-gtk-config/b059801a7025e9fbac623ff75f693cd12bf58470'>Commit.</a>
- Don't display duplicate icon themeicon theme entries. <a href='https://commits.kde.org/kde-gtk-config/60ec53074b9ea3c309ed22c62a71a299c4aa0147'>Commit.</a>
- Keep Cursor and Icon themes lists sorted. <a href='https://commits.kde.org/kde-gtk-config/7b56c23870798adbeb3f110eacae7f1020dcad6f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368600'>#368600</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Media Frame applet's back button behaves correctly again. <a href='https://commits.kde.org/kdeplasma-addons/c9908d08a2813146ef811ea7c7393e54bb5cb22c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13760'>D13760</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Link clipboard sync helper to kcrash. <a href='https://commits.kde.org/kwin/bd5ef5531873acf283fb272048cc42534fa88876'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13753'>D13753</a>
- Avoid potential assert in SM saving. <a href='https://commits.kde.org/kwin/2693e288c57a66facbe6b9932ce8628a53afb246'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395712'>#395712</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13715'>D13715</a>

### <a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a>

- Fix uninstalled run unit tests: set LIBRARY_OUTPUT_DIRECTORY for plugins. <a href='https://commits.kde.org/libkscreen/3bd9546326bbde5df23713f6bd5b69b6aa282f29'>Commit.</a>

### <a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a>

- Monitor document title. <a href='https://commits.kde.org/plasma-browser-integration/49bd13222b1adfc2ca76699d153f5226269d963e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395393'>#395393</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [Icons KCM] Clip delegate during animation. <a href='https://commits.kde.org/plasma-desktop/c24489b5e6cbd19b598dd9d801b63ca5c14d8116'>Commit.</a>
- [Icons KCM] Load preview pixmaps for animation on-demand and cache them. <a href='https://commits.kde.org/plasma-desktop/d8b6f90e2ddb49cc8a800ff461701a5d8c163e01'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13793'>D13793</a>
- [Font Management KCM] Only show on xcb platform. <a href='https://commits.kde.org/plasma-desktop/a446bf0fb1d4fa96eeabf72ab05532fa531fb88b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364746'>#364746</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13920'>D13920</a>
- [Fonts KCM] Generate font rendering previews only on X. <a href='https://commits.kde.org/plasma-desktop/3fd0bf5cef8af100932cd1284fbffa8014326374'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396214'>#396214</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13919'>D13919</a>
- Correct Folder View sizing and representation switch behavior. <a href='https://commits.kde.org/plasma-desktop/4ad27c62b5e71dcf143c5e5a96d9127de38c7d2d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395477'>#395477</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13870'>D13870</a>
- [Splash Screen KCM] Fix "no thumbnail" icon for "None". <a href='https://commits.kde.org/plasma-desktop/50b1acf4016a126b07a25315eeaceb507e16ad92'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13864'>D13864</a>
- Fix crash on post-initial refresh(). <a href='https://commits.kde.org/plasma-desktop/e1252c6e40ac540519e5088111e2f077546ada30'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13856'>D13856</a>
- [Icons KCM] Also set minimum width for icon preview. <a href='https://commits.kde.org/plasma-desktop/a704f3fa50f03bc65f35e2aad11d4ea98487081f'>Commit.</a>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

- [QDBusMenuBar] Guard m_window with a QPointer. <a href='https://commits.kde.org/plasma-integration/73eeda3a7dfb3b155a6198ff733e5ab2f1a89f0e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376340'>#376340</a>. Fixes bug <a href='https://bugs.kde.org/379719'>#379719</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13774'>D13774</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Don't block startplasma sending DBus call to KSplash. <a href='https://commits.kde.org/plasma-workspace/0470689f03de1a0b4ef0ba9eb21be81cf34596b3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13863'>D13863</a>
- [Places Runner] Add place category as subtext. <a href='https://commits.kde.org/plasma-workspace/f896fb554e8e52ca47216496f6828acc2b38e36a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13844'>D13844</a>
- [Places Runner] Fix opening search and timeline URLs. <a href='https://commits.kde.org/plasma-workspace/5a951e9a7eb8bdbf8530e7e8e24a26fda8603ebc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13841'>D13841</a>
- [Places Runner] Fix opening devices. <a href='https://commits.kde.org/plasma-workspace/12b3fb2e3b8165e1e15a1d501507bdf873b1dd98'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13840'>D13840</a>

### <a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a>

- Remove empty Category from desktop file. <a href='https://commits.kde.org/polkit-kde-agent-1/f2c5c6d26fd9f90bc413a0f407d675189a648020'>Commit.</a>
- Add a window icon for authentication dialog under Wayland. <a href='https://commits.kde.org/polkit-kde-agent-1/92b6a3459d4e5ea327e90f5f673594a0472ddb7d'>Commit.</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Create stream once we are supposed to start a stream. <a href='https://commits.kde.org/xdg-desktop-portal-kde/78046e8203cdd651c7bd5cd230aedf7329ca0426'>Commit.</a>