2005-12-03 18:35 +0000 [r485303]  reitelbach

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.cpp: Fix bug 117577
	  (missing i18n calls). This will introduce one new String.
	  BUG:117577 CCMAIL:kde-i18n-doc@kde.org

2005-12-04 00:40 +0000 [r485366]  mhunter

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.cpp: Make consistent
	  with existing string - should prevent fuzzy if I got there in
	  time CCMAIL:kde-i18n-doc@kde.org

2005-12-04 00:50 +0000 [r485368]  mhunter

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.cpp: Okay - maybe it
	  won't CCMAIL:kde-i18n-doc@kde.org

2005-12-04 20:53 +0000 [r485541]  aacid

	* branches/KDE/3.5/kdeedu/kstars/kstars/jupitermoons.h: fix apidox
	  @param

2005-12-06 15:25 +0000 [r486031]  mueller

	* branches/KDE/3.5/kdeedu/kmplot/kmplot/diagr.cpp: fix undefined
	  operation

2005-12-07 23:00 +0000 [r486475]  paolini

	* branches/KDE/3.5/kdeedu/kig/modes/mode.cc,
	  branches/KDE/3.5/kdeedu/kig/modes/construct_mode.cc: This fixes
	  bug n. 98106 (crash)

2005-12-07 23:59 +0000 [r486495]  paolini

	* branches/KDE/3.5/kdeedu/kig/objects/locus_imp.cc: Fix getParam
	  method for a locus (now based on the golden-ratio method)

2005-12-11 07:34 +0000 [r487598]  paolini

	* branches/KDE/3.5/kdeedu/kig/objects/cubic_imp.cc: cartesian
	  equation for cubics had arguments %1, %10m which are too many for
	  QString(s).

2005-12-11 07:51 +0000 [r487599]  paolini

	* branches/KDE/3.5/kdeedu/kig/objects/cubic_imp.cc: Since we
	  already changed the i18n string lets also add two missing spaces

2005-12-12 01:47 +0000 [r487802]  hedlund

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvoctraintable.cpp,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvoctraintable.h:
	  Make keyboard layout switching work again. BUG:117391
	  CCMAIL:kde-edu@kde.org

2005-12-12 18:11 +0000 [r487967]  paolini

	* branches/KDE/3.5/kdeedu/kig/kig/aboutdata.h: fixed email
	  addresses

2005-12-15 16:07 +0000 [r488720]  annma

	* branches/KDE/3.5/kdeedu/kbruch/src/kbruch.cpp,
	  branches/KDE/3.5/kdeedu/kbruch/src/taskvieweroptionsbase.ui: fix
	  Configure dialog which did not resize correctly (when minimized,
	  the last checkbox was hidden) correct (c) year to 2005

2005-12-15 16:23 +0000 [r488723]  annma

	* branches/KDE/3.5/kdeedu/doc/kbruch/convert.png,
	  branches/KDE/3.5/kdeedu/doc/kbruch/settings.png,
	  branches/KDE/3.5/kdeedu/doc/kbruch/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kbruch/checked.png,
	  branches/KDE/3.5/kdeedu/doc/kbruch/compare.png,
	  branches/KDE/3.5/kdeedu/doc/kbruch/factorize.png,
	  branches/KDE/3.5/kdeedu/doc/kbruch/gui_main.png: update
	  screenshots to current defaults CCMAIL=seb.stein@hpfsc.de

2005-12-15 17:06 +0000 [r488729]  annma

	* branches/KDE/3.5/kdeedu/doc/kbruch/reduced.png: update last
	  screenshot, thanks to pinotree for guidance!

2005-12-15 20:36 +0000 [r488762]  annma

	* branches/KDE/3.5/kdeedu/doc/klatin/results.png,
	  branches/KDE/3.5/kdeedu/doc/klatin/klatin1.png,
	  branches/KDE/3.5/kdeedu/doc/klatin/klatin2.png,
	  branches/KDE/3.5/kdeedu/doc/klatin/klatin3.png,
	  branches/KDE/3.5/kdeedu/doc/klatin/klatin4.png: update
	  screenshots to current style && win deco && color
	  CCMAIL=gwright@kde.org

2005-12-15 20:44 +0000 [r488764]  annma

	* branches/KDE/3.5/kdeedu/doc/kiten/kiten1.png,
	  branches/KDE/3.5/kdeedu/doc/kiten/kiten2.png: update screenshots
	  to current theme&style&win deco CCMAIL=jason@katzbrown.com

2005-12-15 21:03 +0000 [r488767]  annma

	* branches/KDE/3.5/kdeedu/doc/kmplot/main.png,
	  branches/KDE/3.5/kdeedu/doc/kmplot/settingsdlg.png,
	  branches/KDE/3.5/kdeedu/doc/kmplot/functionsdlg.png: update
	  screenshots to current style/theme and win deco
	  CCMAIL=f_edemar@linux.se

2005-12-15 22:01 +0000 [r488788]  annma

	* branches/KDE/3.5/kdeedu/doc/kpercentage/main.png,
	  branches/KDE/3.5/kdeedu/doc/kpercentage/help.png,
	  branches/KDE/3.5/kdeedu/doc/kpercentage/welcome.png,
	  branches/KDE/3.5/kdeedu/doc/kpercentage/answer.png: update
	  screenshots to current style (plastik) CCMAIL=bmlmessmer@web.de

2005-12-15 22:26 +0000 [r488793]  annma

	* branches/KDE/3.5/kdeedu/doc/kturtle/wrapping.png,
	  branches/KDE/3.5/kdeedu/doc/kturtle/mainwindow.png,
	  branches/KDE/3.5/kdeedu/doc/kturtle/mainwindow_flower_nrs.png:
	  update screenshots according to current theme/style (plastik)

2005-12-16 01:00 +0000 [r488822]  annma

	* branches/KDE/3.5/kdeedu/doc/keduca/screenshot.png: update
	  screenshot to current theme/style (plastik)

2005-12-17 15:01 +0000 [r489172]  annma

	* branches/KDE/3.5/kdeedu/doc/kalzium/index.docbook: sanitize
	  without ading any new strings

2005-12-17 22:42 +0000 [r489270]  paolini

	* branches/KDE/3.5/kdeedu/kig/modes/label.cc,
	  branches/KDE/3.5/kdeedu/kig/objects/object_factory.cc,
	  branches/KDE/3.5/kdeedu/kig/objects/object_factory.h:
	  Redefinition of a label did not use the new attach method, this
	  caused strange behaviour and even crashes when trying to redefine
	  a label and reattaching it to an angle (or a polygon).

2005-12-19 08:21 +0000 [r489592]  lueck

	* branches/KDE/3.5/kdeedu/doc/kmplot/man-kmplot.1.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/introduction.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/dcop.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/developer.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/using.docbook,
	  branches/KDE/3.5/kdeedu/doc/kturtle/getting-started.docbook,
	  branches/KDE/3.5/kdeedu/doc/kturtle/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/credits.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/reference.docbook,
	  branches/KDE/3.5/kdeedu/doc/kwordquiz/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/commands.docbook,
	  branches/KDE/3.5/kdeedu/doc/kturtle/using-kturtle.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/menu.docbook (removed),
	  branches/KDE/3.5/kdeedu/doc/kmplot/firststeps.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/configuration.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/index.docbook: documentation
	  update (menus, checked guiitems, sanitized) BUG:105193
	  CCMAIL:peter.hedlund@kdemail.net CCMAIL:f_edemar@linux.se
	  CCMAIL:annma.kde.org

2005-12-19 20:47 +0000 [r489818]  adridg

	* branches/KDE/3.5/kdeedu/kstars/kstars/indi/Makefile.am: Remove
	  another fantastically bad idea. Hard-coding OS-specific
	  libraries, that is. CCMAIL: kde@freebsd.org

2005-12-21 05:11 +0000 [r490208]  mueller

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/query-dialogs/ArtQueryDlg.cpp,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvoctrain.cpp:
	  actually react to user input

2005-12-21 22:16 +0000 [r490448]  mhunter

	* branches/KDE/3.5/kdeedu/kmplot/kmplot/settingspagecolor.ui,
	  branches/KDE/3.5/kdeedu/kanagram/src/mainsettingswidget.ui,
	  branches/KDE/3.5/kdeedu/kanagram/src/mainsettings.cpp,
	  branches/KDE/3.5/kdeedu/kig/kig/aboutdata.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/telescopewizard.ui:
	  Typographical corrections and changes CCMAIL:kde-i18n-doc@kde.org

2005-12-24 01:10 +0000 [r490980]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/data/image_url.dat: Thanks
	  for the report, fixed. BUG: 118917

2005-12-24 17:11 +0000 [r491124]  annma

	* branches/KDE/3.5/kdeedu/doc/klettres/index.docbook: sanitize

2005-12-24 17:17 +0000 [r491125]  annma

	* branches/KDE/3.5/kdeedu/doc/khangman/index.docbook: sanitize

2005-12-25 12:37 +0000 [r491304]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/brazil.kgm,
	  branches/KDE/3.5/kdeedu/kgeography/src/mapsdatatranslation.cpp:
	  Typo fixing BUGS: 118966 CCMAIL: kde-i18n-doc@kde.org

2005-12-25 22:08 +0000 [r491378]  annma

	* branches/KDE/3.5/kdeedu/doc/kturtle/index.docbook: sanitize

2005-12-25 22:16 +0000 [r491381]  annma

	* branches/KDE/3.5/kdeedu/doc/keduca/index.docbook: sanitize

2005-12-26 18:21 +0000 [r491597-491596]  pino

	* branches/KDE/3.5/kdeedu/doc/kalzium/index.docbook: Fix a pair of
	  typos. CCMAIL: kde-i18n-doc@kde.org Carsten, could you please
	  forward port it to trunk, please? CCMAIL: Carsten Niehaus
	  <cniehaus@gmx.de>

	* branches/KDE/3.5/kdeedu/libkdeedu/kdeeduplot/kplotobject.cpp:
	  Warning messages on console doesn't need to be translated.
	  CCMAIL: kde-i18n-doc@kde.org

2005-12-27 23:22 +0000 [r491903-491901]  annma

	* branches/KDE/3.5/kdeedu/kanagram/src/kanagram.desktop: add
	  DocPath entry for KHelpCenter thanks to mpyne and toma!

	* branches/KDE/3.5/kdeedu/blinken/src/blinken.desktop: add DocPath
	  entry for KHelpCenter thanks to mpyne and toma!

2005-12-27 23:27 +0000 [r491904]  annma

	* branches/KDE/3.5/kdeedu/kgeography/src/kgeography.desktop: add
	  DocPath here as well

2005-12-28 22:51 +0000 [r492155]  annma

	* branches/KDE/3.5/kdeedu/doc/kgeography/index.docbook: bug 115344
	  BUG=115344

2005-12-30 09:54 +0000 [r492531]  cniehaus

	* branches/KDE/3.5/kdeedu/kalzium/src/molcalcwidget.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/moleculeparser.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/molcalcwidget.h,
	  branches/KDE/3.5/kdeedu/kalzium/src/moleculeparser.h: This fixes
	  bug #117774 The calculator has two issues. First, it didn't tell
	  the user when the string was invalid (eg H2Ojdfkjd). Second, it
	  didn't stop calculating in some cases (H2g produces a different
	  error than H2Og, only the first stopped the calculation). With
	  this commit I am introducing a new string "Invalid input" which
	  is displayed when the parser notices an error.
	  CCMAIL:kalzium@kde.org BUG:117774

2005-12-30 12:44 +0000 [r492594]  cniehaus

	* branches/KDE/3.5/kdeedu/kalzium/src/moleculeparser.h: Adding
	  apidox-wrappers so that the errors are caught on ebn To check for
	  missing APIDOX: [13:44] <adridg> DOXYGEN_SET_WARN_IF_UNDOCUMENTED
	  = YES [13:44] <adridg> DOXYGEN_SET_INTERNAL_DOCS = YES

2005-12-30 22:01 +0000 [r492725]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/elementdataviewer.cpp: Fix Y
	  padding in Plot Viewer so it's possible to plot single points.

2005-12-30 23:49 +0000 [r492751]  thiago

	* branches/KDE/3.5/kdeedu/libkdeedu/extdate/extdatetime.h: Removing
	  stray tab from before #. I'm not sure preprocessor directives are
	  allowed to start beyond the first column. BUG:119275

2006-01-02 14:20 +0000 [r493448]  scripty

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-delete_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-set_language.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-sort_incr.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-insert_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-delete_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-sort_incr.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-sort_incr.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-delete_table_col.png,
	  branches/KDE/3.5/kdeedu/kmplot/icons/actions/cr32-action-editconstants.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-sort_num.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-sort_num.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-insert_table_col.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-rand_less.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-insert_table_col.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-rand_less.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-rand_less.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-run_multi.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-run_multi.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-run_multi.png,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/kfeeder16.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-edit_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-run_query.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-run_query.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-statistics.png,
	  branches/KDE/3.5/kdeedu/kwordquiz/src/pics/cr16-action-insert_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-run_query.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-set_language.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-delete_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-insert_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-insert_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-configure_query.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-sort_num.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-delete_table_col.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-configure_query.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-insert_table_col.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-delete_table_col.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-edit_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-edit_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr22-action-cleanup.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-cleanup.png,
	  branches/KDE/3.5/kdeedu/kwordquiz/src/pics/cr16-action-delete_table_row.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-statistics.png,
	  branches/KDE/3.5/kdeedu/kwordquiz/src/pics/cr16-action-sort_incr.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr16-action-statistics.png,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/pics/cr32-action-set_language.png:
	  Remove svn:executable from some typical non-executable files
	  (goutte)

2006-01-02 20:25 +0000 [r493649]  pino

	* branches/KDE/3.5/kdeedu/libkdeedu/kdeeduplot/kplotwidget.cpp: Fix
	  grid painting

2006-01-03 10:27 +0000 [r493777]  cniehaus

	* branches/KDE/3.5/kdeedu/kalzium/src/data/data.xml: CCBUG:119430

2006-01-04 11:51 +0000 [r494233]  cniehaus

	* branches/KDE/3.5/kdeedu/kalzium/src/element.cpp: * Autsch. Of
	  course both melting- AND boilingpoint needs to be < 295.0 Kelvin.
	  Some elements have an known boiling- but unknown meltingpoint or
	  vice-versa. Therefore, those elements where displayed as gasoline
	  even though (for example) the meltingpoint was something like a
	  1000 Kelvin...

2006-01-07 23:54 +0000 [r495397]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/orbitswidget.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/orbitswidget.h: Don't add new
	  items to the hull list every time we change element.

2006-01-10 14:50 +0000 [r496471]  mueller

	* branches/KDE/3.5/kdeedu/kstars/kstars/indi/fli/libfli-parport.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/indi/fli/libfli-usb.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/indi/fli/libfli-sys.h:
	  support for GNU/kfreeBSD by Aurelien Jarno <aurelien@aurel32.net>

2006-01-13 02:53 +0000 [r497554]  hedlund

	* branches/KDE/3.5/kdeedu/kwordquiz/src/kwordquiz.cpp: Don't crash
	  when downloading vocabularies. BUG:119899

2006-01-15 12:01 +0000 [r498303]  cniehaus

	* branches/KDE/3.5/kdeedu/kalzium/src/main.cpp: New version number
	  for KDE 3.5.1

2006-01-15 22:21 +0000 [r498683]  paolini

	* branches/KDE/3.5/kdeedu/kig/misc/common.cpp: Contains test didn't
	  work for the origin of a ray, fixed

2006-01-16 16:40 +0000 [r498920]  cniehaus

	* branches/KDE/3.5/kdeedu/kalzium/src/data/data.xml: * New data,
	  reference: The Elements', John Emsley, Clarendon Oxford, 1989
	  (ISBN 0-19-855237-8) and 'Periodic Table of the Elements', Fluck
	  and Heumann, Wiley-VCH, 1997 (ISBN- 3-527-10109-8), based on
	  IUPAC recommendations of 1995. Thanks, Vincent Patrick

2006-01-19 12:11 +0000 [r500026]  cniehaus

	* branches/KDE/3.5/kdeedu/kalzium/src/data/data.xml: Add atomic
	  radius of xenon

