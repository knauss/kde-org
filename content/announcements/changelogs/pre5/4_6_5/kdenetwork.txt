------------------------------------------------------------------------
r1236076 | lvsouza | 2011-06-11 17:31:03 +1200 (Sat, 11 Jun 2011) | 4 lines

Backport r1236075:

Prevents crash when closing Kopete with kwallet closed.

------------------------------------------------------------------------
r1238773 | scripty | 2011-06-29 15:34:37 +1200 (Wed, 29 Jun 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
