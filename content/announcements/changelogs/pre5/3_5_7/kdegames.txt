2007-03-08 20:28 +0000 [r640692]  lueck

	* branches/KDE/3.5/kdegames/kbackgammon/kbg.h,
	  branches/KDE/3.5/kdegames/kbackgammon/kbackgammonui.rc,
	  branches/KDE/3.5/kdegames/kbackgammon/version.h,
	  branches/KDE/3.5/kdegames/kbackgammon/kbg.cpp,
	  branches/KDE/3.5/kdegames/kbackgammon/main.cpp: make command
	  toolbar usable again, remove obsolete urls

2007-03-17 21:21 +0000 [r643583]  aacid

	* branches/KDE/3.5/kdegames/ksokoban/Makefile.am: this seems to
	  help with a rebuilding bug, confirmed by amantia and me

2007-03-29 14:13 +0000 [r647780]  binner

	* branches/KDE/3.5/kdegames/kreversi/sounds/eventsrc: fix default
	  sound file names

2007-04-04 12:46 +0000 [r650394]  mueller

	* branches/KDE/3.5/kdegames/kwin4/kwin4/kspritecache.h,
	  branches/KDE/3.5/kdegames/kbounce/game.h,
	  branches/KDE/3.5/kdegames/lskat/lskat/lskatdoc.h,
	  branches/KDE/3.5/kdegames/kmahjongg/boardwidget.h,
	  branches/KDE/3.5/kdegames/ksnake/board.h,
	  branches/KDE/3.5/kdegames/kenolaba/Board.h,
	  branches/KDE/3.5/kdegames/kjumpingcube/kjumpingcube.h,
	  branches/KDE/3.5/kdegames/kbackgammon/engines/generic/kbgengine.h,
	  branches/KDE/3.5/kdegames/konquest/int_validator.h,
	  branches/KDE/3.5/kdegames/ksnake/snake.h,
	  branches/KDE/3.5/kdegames/kfouleggs/board.h,
	  branches/KDE/3.5/kdegames/libksirtet/lib/mp_interface.h,
	  branches/KDE/3.5/kdegames/ksokoban/ImageData.cpp,
	  branches/KDE/3.5/kdegames/kmahjongg/Tileset.h,
	  branches/KDE/3.5/kdegames/libkdegames/kgame/kgame.h,
	  branches/KDE/3.5/kdegames/kmahjongg/GameTimer.h,
	  branches/KDE/3.5/kdegames/libksirtet/lib/smanager.h,
	  branches/KDE/3.5/kdegames/kmahjongg/Background.h,
	  branches/KDE/3.5/kdegames/kwin4/kwin4/kwin4.h,
	  branches/KDE/3.5/kdegames/kmahjongg/Preview.h,
	  branches/KDE/3.5/kdegames/ksnake/ball.h,
	  branches/KDE/3.5/kdegames/kenolaba/Spy.h,
	  branches/KDE/3.5/kdegames/kjumpingcube/kcubeboxwidget.h,
	  branches/KDE/3.5/kdegames/ksnake/rattler.h,
	  branches/KDE/3.5/kdegames/libksirtet/lib/pline.h,
	  branches/KDE/3.5/kdegames/konquest/gameboard.h,
	  branches/KDE/3.5/kdegames/kmahjongg/BoardLayout.h,
	  branches/KDE/3.5/kdegames/konquest/gamecore.h: pedantic--

2007-04-09 01:26 +0000 [r651729]  ianw

	* branches/KDE/3.5/kdegames/kgoldrunner/gamedata/levels.tar,
	  branches/KDE/3.5/kdegames/kgoldrunner/gamedata/games.dat: State
	  of Terror: a new KGoldrunner game ... just when you thought you
	  were safe ...

2007-04-12 18:04 +0000 [r653143]  capriotti

	* branches/KDE/3.5/kdegames/kbattleship/kbattleship/kbattleshipserver.cpp,
	  branches/KDE/3.5/kdegames/kbattleship/kbattleship/kbattleshipclient.cpp:
	  Fixed parsing problem when more than one message is received at
	  once.

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

