2007-01-24 17:16 +0000 [r626814]  mueller

	* branches/KDE/3.5/kdeaddons/konq-plugins/webarchiver/webarchivecreator.cpp,
	  branches/KDE/3.5/kdeaddons/konq-plugins/webarchiver/archivedialog.cpp:
	  fix a minor but annoying privacy issue: the web archiver would
	  happily put local files or other launch other unsafe protocols if
	  referenced in the website, even though konqueror itself wouldn't
	  handle it

2007-03-07 19:27 +0000 [r640363]  mkoller

	* branches/KDE/3.5/kdeaddons/konq-plugins/validators/validatorsdialog.cpp:
	  BUG: 140539 CCMAIL: hannibal@megapolis.pl correct default URLs

2007-03-14 21:14 +0000 [r642614]  dembinski

	* branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidthview.cc.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/dialogdesign.cc
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/Makefile
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidthview.h.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidth.cc.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/Makefile.in
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_preferencesdialog.cc.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_test.h.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/README
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_dialogdesign.h.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/lo32-app-kbandwidth.png
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/kbandwidthview.cc
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/lo16-app-kbandwidth.png
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/kbandwidthview.h
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/kbandwidth.cc
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/preferencesdialog.cc
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidth.desktop.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/dialogdesign.ui
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/dialogdesign.h
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidth.h.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_preferencesdialog.h.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_Makefile.am.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/cr32-app-kbandwidth.png
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/kbandwidth.desktop
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/cr16-app-kbandwidth.png
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_mypreferencesdialog.h.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/configure.in.in
	  (added), branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth
	  (added), branches/KDE/3.5/kdeaddons/README,
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_AUTHORS.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/dialogdesign.ui~
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_configure.in.bak
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/kbandwidth.h
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/preferencesdialog.h
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/Makefile.am
	  (added): adding kbandwidth to kdeaddons

2007-03-15 00:25 +0000 [r642662-642661]  dembinski

	* branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidthview.cc.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/dialogdesign.cc
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/Makefile
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidthview.h.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/dialogdesign.h
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidth.cc.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/Makefile.in,
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_test.h.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_preferencesdialog.cc.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidth.h.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_preferencesdialog.h.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_Makefile.am.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_mypreferencesdialog.h.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_dialogdesign.h.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_AUTHORS.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/dialogdesign.ui~
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_configure.in.bak
	  (removed),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/.kate_kbandwidth.desktop.bak
	  (removed): removed unnecessary files

	* branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth/Makefile.in
	  (removed): more deleting

2007-03-15 09:30 +0000 [r642728]  coolo

	* branches/KDE/3.5/kdeaddons/kicker-applets/kbandwidth (removed),
	  trunk/kdereview/kbandwidth (added): move kbandwidth out again

2007-03-24 15:54 +0000 [r646108]  callegari

	* branches/KDE/3.5/kdeaddons/kate/cppsymbolviewer/plugin_katesymbolviewer.cpp,
	  branches/KDE/3.5/kdeaddons/kate/cppsymbolviewer/cpp_parser.cpp,
	  branches/KDE/3.5/kdeaddons/kate/cppsymbolviewer/tcl_parser.cpp,
	  branches/KDE/3.5/kdeaddons/kate/cppsymbolviewer/plugin_katesymbolviewer.h:
	  - improved C++ parser. - Java support with C++ parser - option to
	  always expand tree mode - option to enable/disable sorting

2007-04-02 14:17 +0000 [r649317]  aacid

	* branches/KDE/3.5/kdeaddons/noatun-plugins/oblique/base.cpp: build
	  on gcc 4.3 snapshots
	  http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=417271

2007-04-04 12:46 +0000 [r650395]  mueller

	* branches/KDE/3.5/kdeaddons/kicker-applets/math/parser.h,
	  branches/KDE/3.5/kdeaddons/kate/tabbarextension/plugin_katetabbarextension.h,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/configdialog.h,
	  branches/KDE/3.5/kdeaddons/kate/cppsymbolviewer/plugin_katesymbolviewer.h,
	  branches/KDE/3.5/kdeaddons/kate/filetemplates/plugin/filetemplates.cpp,
	  branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/mediacontrol.cpp,
	  branches/KDE/3.5/kdeaddons/kate/make/plugin_katemake.cpp,
	  branches/KDE/3.5/kdeaddons/kate/insertcommand/plugin_kateinsertcommand.h,
	  branches/KDE/3.5/kdeaddons/atlantikdesigner/designer/editor.h,
	  branches/KDE/3.5/kdeaddons/kate/filetemplates/plugin/filetemplates.h,
	  branches/KDE/3.5/kdeaddons/kate/snippets/csnippet.h,
	  branches/KDE/3.5/kdeaddons/konq-plugins/dirfilter/dirfilterplugin.h,
	  branches/KDE/3.5/kdeaddons/konq-plugins/fsview/scan.h: pedantic--

2007-04-04 19:22 +0000 [r650534]  lueck

	* branches/KDE/3.5/kdeaddons/doc/ksig/index.docbook (added),
	  branches/KDE/3.5/kdeaddons/doc/ksig/Makefile.am (added),
	  branches/KDE/3.5/kdeaddons/doc/ksig (added),
	  branches/KDE/3.5/kdeaddons/doc/ksig/screenshot.png (added):
	  documentation backport from trunk

2007-04-04 20:40 +0000 [r650565]  lueck

	* branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/kolourpicker.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/crashes/Makefile.am,
	  branches/KDE/3.5/kdeaddons/konq-plugins/validators/Makefile.am,
	  branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/ktimemon.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/uachanger/Makefile.am,
	  branches/KDE/3.5/kdeaddons/konq-plugins/babelfish/Makefile.am,
	  branches/KDE/3.5/kdeaddons/konq-plugins/khtmlsettingsplugin/Makefile.am,
	  branches/KDE/3.5/kdeaddons/konq-plugins/kimgalleryplugin/Makefile.am,
	  branches/KDE/3.5/kdeaddons/konq-plugins/domtreeviewer/Makefile.am,
	  branches/KDE/3.5/kdeaddons/konq-plugins/webarchiver/Makefile.am,
	  branches/KDE/3.5/kdeaddons/ksig/ksig.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/fsview/Makefile.am,
	  branches/KDE/3.5/kdeaddons/konq-plugins/dirfilter/Makefile.am:
	  make the documentation visible in khelpcenter

2007-04-26 15:22 +0000 [r658262]  schwarzer

	* branches/KDE/3.5/kdeaddons/doc/konq-plugins/kuick/index.docbook:
	  Fixes two typos, introduces two fuzzies for translators I hope
	  this falls into the "clear typos" thing that is still allowed
	  during the string freeze... and don't panic... tagging and
	  release are postponed (for those who didn't read that yet. ;)) -
	  May 14th, 2007: Tagging KDE 3.5.7 - May 22th, 2007: Expected
	  release date of KDE 3.5.7 Thanks Natalie for the report. BUG:
	  144692

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

