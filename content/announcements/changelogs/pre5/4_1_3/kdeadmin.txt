2008-09-25 18:13 +0000 [r864850]  aacid <aacid@localhost>:

	* branches/KDE/4.1/kdeadmin/kuser/ku_user.h,
	  branches/KDE/4.1/kdeadmin/kuser/ku_mainview.cpp: Fix bugs 171488
	  and 171574 Patch by wppan@redflag-linux.com --is line, and those
	  below, will be ignored-- M ku_mainview.cpp M ku_user.h

2008-09-27 15:56 +0000 [r865441]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdeadmin/doc/ksysv/index.docbook,
	  branches/KDE/4.1/kdeadmin/doc/kcontrol/knetworkconf/index.docbook:
	  documentation backport for 4.1.3 from trunk

2008-10-17 06:08 +0000 [r872395]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdeadmin/ksystemlog/src/ksystemlog.desktop,
	  branches/KDE/4.1/kdeadmin/kcron/src/kcm_cron.desktop: SVN_SILENT
	  made messages (.desktop file)

2008-10-22 07:09 +0000 [r874650]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdeadmin/kcron/src/kcm_cron.desktop: SVN_SILENT
	  made messages (.desktop file)

2008-10-22 09:38 +0000 [r874680]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdeadmin/kuser/ku_main.cpp: added missing
	  translations (message catalog) from kdepimlibs/kldap

