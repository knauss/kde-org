2006-10-13 08:25 +0000 [r595065]  micron

	* branches/KDE/3.5/kdeutils/klaptopdaemon/daemondock.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/daemondock.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/battery.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/battery.h: klaptopdaemon
	  NEW FEATURE: optionally show battery level percentage over the
	  systemtray icon. Commit approved by Paul Campbell
	  <paul<at>taniwha.com>

2006-10-16 23:13 +0000 [r596246]  jriddell

	* branches/KDE/3.5/kdeutils/ark/sevenzip.cpp,
	  branches/KDE/3.5/kdeutils/ark/ark.desktop,
	  branches/KDE/3.5/kdeutils/ark/ark_part.desktop: Improved 7zip
	  support from Anthony Mercatante <tonio@ubuntu.com>

2006-10-20 16:33 +0000 [r597519]  mlaurent

	* branches/KDE/3.5/kdeutils/ark/zip.cpp: Now we can add file to zip
	  archive

2006-11-05 22:27 +0000 [r602406]  mkoller

	* branches/KDE/3.5/kdeutils/ark/arkutils.cpp: Include HP-UX patch
	  from bko 103962

2006-11-05 22:31 +0000 [r602410-602408]  mkoller

	* branches/KDE/3.5/kdeutils/kcalc/kcalc_core.cpp: included patch
	  for HP-UX from bko 103962

	* branches/KDE/3.5/kdeutils/kcalc/kcalctype.h: include HP-UX patch
	  from bko 103962

2006-11-05 22:37 +0000 [r602416]  mkoller

	* branches/KDE/3.5/kdeutils/ksim/monitors/filesystem/filesystemstats.cpp:
	  included patch for HP-UX from bko 103962

2006-11-08 23:45 +0000 [r603438]  wstephens

	* branches/KDE/3.5/kdeutils/kgpg/keyservers.cpp: Show keyservers in
	  search dialog on first run, if no server present in
	  .gnupg/options

2006-11-20 21:00 +0000 [r606567]  wirr

	* branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.h,
	  branches/KDE/3.5/kdeutils/superkaramba/examples/template.py:
	  Cleaning up the API Documentation Many thanks to Daniel!

2006-11-22 09:11 +0000 [r606891]  jriddell

	* branches/KDE/3.5/kdeutils/ksim/Makefile.am,
	  branches/KDE/3.5/kdeutils/ksim/ksim.desktop: show .desktop file
	  (in kicker New Panel menu) do not install .desktop file in
	  application menu

2006-12-02 22:48 +0000 [r609975]  mueller

	* branches/KDE/3.5/kdeutils/superkaramba/src/karambaapp.cpp,
	  branches/KDE/3.5/kdeutils/kwallet/allyourbase.cpp: fix compiler
	  warnings

2006-12-04 22:55 +0000 [r610681]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/knumber/knumber_priv.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdeutils/kcalc/knumber/knumber_priv.h: Further
	  fixes for 64-bit CPUs

2006-12-04 23:47 +0000 [r610685]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp: Small fix for
	  input '123.e' is turned into '123e'

2006-12-05 00:43 +0000 [r610694]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp: Fix BUG 137963

2006-12-05 19:25 +0000 [r610844]  esken

	* branches/KDE/3.5/kdeutils/kmilo/thinkpad/thinkpad.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/delli8k/delli8k.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/kmilo_kvaio/kvaio.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/generic/generic_monitor.cpp:
	  KMilo now uses the new KMix DCOP interface for muting master.
	  Aids in fixing a KMilo/KMix interoperability issue. This fixes
	  two bugs: BUGS: 134820 BUGS: 134604

2006-12-10 20:52 +0000 [r612312]  wirr

	* branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/themelocale.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/themelocale.h: Fixing
	  a unicode bug BUG: 117272 BUG: 114553 Fixing infinite theme
	  reloading when a theme is changed BUG: 122236

2007-01-03 00:49 +0000 [r619230]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.h: Added a
	  constant for NaN.

2007-01-03 01:29 +0000 [r619236]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp: First step for
	  fixing BUGs 137137 and 133421

2007-01-05 00:21 +0000 [r620008]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp: Fix BUG 133421:
	  kcalc does not paste all hex values correctly

2007-01-05 00:51 +0000 [r620019]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.cpp: Effect of
	  constructor KNumber(QString) was not defined, when QString was
	  not a valid number!!

2007-01-05 01:10 +0000 [r620021]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.h: Fix Bug 137137:
	  modulus of a pasted number is always 0 and Bug 137585: pi
	  calculation gives errors on its value and pastes extra places

2007-01-09 21:01 +0000 [r621826]  lueck

	* branches/KDE/3.5/kdeutils/kdelirc/irkick/irkick.desktop: make the
	  docs visible in khelpcenter

2007-01-09 21:47 +0000 [r621843]  binner

	* branches/KDE/3.5/kdeutils/superkaramba/src/karambasessionmanaged.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karambasessionmanaged.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/main.cpp: fix session
	  management on -Bdirect linking machines

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

