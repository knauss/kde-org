2005-03-04 13:55 +0000 [r394856]  pino

	* kget/kget_plug_in/kget_linkview.cpp: Backporting implementation
	  wish #77398 (wishlist): Not apparent which column the list is
	  sorted by BUGS: 77398

2005-03-04 22:03 +0000 [r394940]  pino

	* kget/dlgPreferences.cpp, kget/dlgPreferences.h: Backporting fix
	  for bug #56997 (normal): settings saved on click on cancel button
	  BUGS: 56997

2005-03-05 16:33 +0000 [r395074]  pino

	* kget/kgetui.rc: Removed the duplicate 'drop_target' action. I
	  left only the one in the Option menu, it seemed (for me) more
	  logic to have such action at least in the Option menu, not in
	  View.

2005-03-05 17:30 +0000 [r395091]  pino

	* kget/docking.cpp, kget/droptarget.h, kget/kmainwidget.cpp,
	  kget/droptarget.cpp: Backporting the implementation of wishlist
	  #96190: ability to close drop target via context menu I can close
	  it, now. BUGS: 96190

2005-03-06 00:25 +0000 [r395168]  pino

	* kget/kget_plug_in/kget_plug_in.cpp: qDebug() => kdDebug()

2005-03-07 12:46 +0000 [r395466]  boiko

	* kopete/protocols/oscar/liboscar/ssimodifytask.cpp,
	  kopete/protocols/oscar/aim/aimcontact.cpp,
	  kopete/protocols/oscar/liboscar/ssilisttask.cpp,
	  kopete/protocols/oscar/liboscar/ssiauthtask.cpp,
	  kopete/protocols/oscar/icq/icqcontact.cpp: Backporting fix for
	  #99671 (AIM contacts with blank spaces were not shown as online).

2005-03-07 12:58 +0000 [r395468]  wstephens

	* kopete/plugins/nowlistening/nlkscd.cpp: Backport fix getting
	  artist/album info from kscd

2005-03-07 13:01 +0000 [r395470]  wstephens

	* kopete/protocols/groupwise/gwmessagemanager.cpp: Show logging
	  icon in groupwise chats.

2005-03-12 04:34 +0000 [r396895]  mattr

	* kopete/protocols/oscar/liboscar/offlinemessagestask.cpp: Backport
	  solaris compile fix. Should compile cleanly in kde 3.4.1 or a cvs
	  checkout of KDE/3.4 after 2005.03.11 CCBUGS: 99540

2005-03-14 14:24 +0000 [r397537]  wstephens

	* kopete/kopete/chatwindow/kopetechatwindow.h,
	  kopete/kopete/kopeteapplication.cpp,
	  kopete/kopete/chatwindow/Makefile.am,
	  kopete/kopete/kopetewindow.cpp,
	  kopete/kopete/kopeteapplication.h,
	  kopete/libkopete/kopeteprotocol.cpp,
	  kopete/kopete/kopetewindow.h, kopete/kopete/systemtray.cpp,
	  kopete/kopete/chatwindow/kopetechatwindow.cpp,
	  kopete/libkopete/kopetepluginmanager.cpp: Backport fix for
	  crashing on session exit - unload plugins properly. BUG:91288

2005-03-15 14:30 +0000 [r397816-397815]  wstephens

	* kopete/protocols/oscar/liboscar/client.cpp,
	  kopete/protocols/oscar/liboscar/client.h,
	  kopete/protocols/oscar/liboscar/typingnotifytask.cpp: Backport
	  typing notifications fix

	* kopete/protocols/oscar/oscarcontact.h,
	  kopete/protocols/oscar/oscaraccount.h,
	  kopete/protocols/oscar/oscarcontact.cpp,
	  kopete/protocols/oscar/oscaraccount.cpp: Backport typing
	  notifications fix CCBUGS:101037

2005-03-15 22:30 +0000 [r397926]  dmacvicar

	* kopete/protocols/jabber/ui/jabbereditaccountwidget.cpp: backport:
	  Fix: can't specify custom server port (textedit box disabled)
	  CCBUGS: 100420

2005-03-15 22:33 +0000 [r397927]  dmacvicar

	* kopete/kopete/chatwindow/chattexteditpart.cpp: Backport bugfix:
	  Holding enter sends empty messages CCBUGS: 100334

2005-03-15 23:04 +0000 [r397945]  wstephens

	* kopete/protocols/oscar/oscarcontact.h,
	  kopete/protocols/oscar/liboscar/typingnotifytask.h,
	  kopete/protocols/oscar/liboscar/client.cpp,
	  kopete/protocols/oscar/liboscar/client.h,
	  kopete/protocols/oscar/oscarcontact.cpp,
	  kopete/protocols/oscar/liboscar/typingnotifytask.cpp,
	  kopete/protocols/oscar/oscaraccount.cpp: Backport fix for sending
	  typing notifications in ICQ CCBUG:101037

2005-03-16 09:08 +0000 [r398020]  ogoffart

	* kopete/protocols/msn/ui/msneditaccountwidget.cpp: Backport the
	  fix for the Bug 101047

2005-03-16 09:29 +0000 [r398023]  ogoffart

	* kopete/protocols/msn/msnswitchboardsocket.cpp: Backport fix for
	  Bug 100577: kopete does not show custom font colour, font size or
	  font attributes, e.g. underline, bold... (in a MSN conversation)

2005-03-17 01:48 +0000 [r398253]  mattr

	* kopete/protocols/oscar/aim/aimaccount.cpp: Backport AIM HTML
	  background color parsing for font tags

2005-03-17 15:37 +0000 [r398406]  lunakl

	* kopete/libkopete/kopetenotifyclient.cpp: Backport r1.19.

2005-03-18 10:38 +0000 [r398659]  porten

	* kppp/ChangeLog, kppp/connect.cpp: integreated flow control fix
	  from HEAD

2005-03-18 12:47 +0000 [r398688]  wstephens

	* kopete/kopete/chatwindow/kopetechatwindow.cpp: Backport fix for
	  crash when reopening closed tabbed chatwindows.

2005-03-20 00:57 +0000 [r399115]  wstephens

	* kopete/protocols/groupwise/ui/gweditaccountwidget.cpp: Backport
	  fix to tab order

2005-03-21 20:51 +0000 [r399547]  claudiu

	* kppp/Rules/Romania/Easynet.rst, kppp/Rules/Romania/ClickNet.rst
	  (added), kppp/Rules/Romania/Romtelecom_Interjudetean.rst: CCMAIL:
	  "Sorin B." <sorin@bonbon.net> Added updated Romanian KPPP rules
	  and new one from Sorin. Thanks!

2005-03-22 13:57 +0000 [r399712]  wstephens

	* kopete/protocols/groupwise/libgroupwise/client.cpp,
	  kopete/protocols/groupwise/libgroupwise/userdetailsmanager.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/needfoldertask.cpp,
	  kopete/protocols/groupwise/gwaccount.cpp,
	  kopete/protocols/groupwise/libgroupwise/coreprotocol.h,
	  kopete/protocols/groupwise/libgroupwise/tasks/conferencetask.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/logintask.cpp,
	  kopete/protocols/groupwise/libgroupwise/responseprotocol.cpp,
	  kopete/protocols/groupwise/libgroupwise/gwfield.cpp,
	  kopete/protocols/groupwise/libgroupwise/inputprotocolbase.h,
	  kopete/protocols/groupwise/libgroupwise/userdetailsmanager.h,
	  kopete/protocols/groupwise/libgroupwise/tasks/conferencetask.h,
	  kopete/protocols/groupwise/libgroupwise/tasks/createcontacttask.cpp,
	  kopete/protocols/groupwise/libgroupwise/gwclientstream.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/eventtask.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/movecontacttask.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/statustask.cpp,
	  kopete/protocols/groupwise/libgroupwise/gwerror.h,
	  kopete/protocols/groupwise/libgroupwise/tasks/joinconferencetask.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/requesttask.cpp,
	  kopete/protocols/groupwise/libgroupwise/task.cpp,
	  kopete/protocols/groupwise/libgroupwise/eventprotocol.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/connectiontask.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/modifycontactlisttask.cpp,
	  kopete/protocols/groupwise/libgroupwise/coreprotocol.cpp,
	  kopete/protocols/groupwise/libgroupwise/inputprotocolbase.cpp:
	  Backport debugging output cleanup

2005-03-23 10:39 +0000 [r399954]  pino

	* kget/kget_plug_in/kget_plug_in.rc, kget/logwindow.cpp: * Two
	  small improvements in logwindow.cpp * Activate KGet submenu in
	  Konqueror menu bar.

2005-03-23 14:47 +0000 [r400006]  wstephens

	* kopete/protocols/groupwise/libgroupwise/client.cpp,
	  kopete/protocols/groupwise/libgroupwise/responseprotocol.cpp,
	  kopete/protocols/groupwise/libgroupwise/eventprotocol.cpp:
	  Backport: Fix wrong arg strings for QString::arg()

2005-03-23 15:12 +0000 [r400012]  wstephens

	* kopete/protocols/oscar/oscarcontact.cpp: Backport correct
	  escaping for AIM messages BUG:99569

2005-03-24 23:45 +0000 [r400370]  neundorf

	* lanbrowsing/kio_lan/kio_lan.cpp: -fix kio_lan crash (I never
	  noticed it was crashing):
	  http://lists.kde.org/?l=kde-core-devel&m=111169752824087&w=2 Alex

2005-03-25 16:47 +0000 [r400565]  mattr

	* kopete/protocols/oscar/oscarcontact.cpp: Use the escaped body of
	  the message all the time. Fixes #102383. The fix will be in KDE
	  3.4.1 BUG: 102383

2005-03-27 09:51 +0000 [r400930]  loschwitz

	* debian/changelog, debian/kppp.README.Debian, debian/krdc.install,
	  debian/krfb.install, debian/rules, debian/kopete.install,
	  debian/lisa.install, debian/knewsticker.install,
	  debian/kget.install, debian/librss1-dev.install,
	  debian/kdict.install, debian/copyright,
	  debian/kwifimanager.install, debian/kppp.install,
	  debian/ktalkd.install, debian/ksirc.install, debian/control,
	  debian/kopete.manpages, debian/dcoprss.install,
	  debian/librss1.install, debian/kpf.install: Updated to the
	  kdenetwork packages by the KDE Maintainers Group as of 20030327

2005-03-27 16:43 +0000 [r401035]  deller

	* wifi/interface_wireless_wirelessextensions.cpp: backport crash
	  fix: do not crash when used with older wifi cards which generate
	  a wifiX and a wlanX device.

2005-03-27 18:02 +0000 [r401045]  loschwitz

	* debian/cdbs (added), debian/cdbs/kde.mk (added): Checking the
	  cdbs directory in

2005-03-27 18:05 +0000 [r401050-401048]  loschwitz

	* debian/kppp.lintian (added), debian/ksirc.lintian (added),
	  debian/kdenetwork-kfile-plugins.install (added),
	  debian/kopete.postinst (added),
	  debian/kdenetwork-filesharing.install (added),
	  debian/kopete.prerm (added), debian/kppp-options (added),
	  debian/kopete.README.Debian (added): Checking in the second batch
	  of files as of 20050327

	* debian/patches (added), debian/patches/common/update.sh (added),
	  debian/patches/common/05_pedantic-errors.diff (added),
	  debian/patches/common/07_disable_no_undefined.diff (added),
	  debian/patches/common/README (added),
	  debian/patches/common/03_libtool_update.diff (added),
	  debian/patches/common (added),
	  debian/patches/common/04_am_maintainer_mode.diff (added),
	  debian/patches/common/02_autotools_update.diff (added),
	  debian/patches/common/06_automake-1.9.diff (added): Adding the
	  third batch of files as of 20050327

	* debian/patches/07_xlibs-static-pic.diff (added),
	  debian/patches/10_kppp_options.diff (added),
	  debian/patches/98_buildprep.diff (added),
	  debian/patches/001-kdenetwork-branch_20050326.diff (added):
	  Adding yet another batch of files as of 20050327

2005-03-28 13:04 +0000 [r401250]  dfaure

	* ktalkd/ktalkd/process.cpp: backport fix for 102604: ktalkd cannot
	  find the correct user - uninitialized variable

2005-03-28 19:02 +0000 [r401354]  gianni

	* kppp/Rules/Italy/Tele2_Internet.rst: new price

2005-03-29 10:39 +0000 [r401564]  gianni

	* kppp/Rules/Italy/Tele2_Internet_Tele2.rst (added),
	  kppp/Rules/Italy/Makefile.am, kppp/Rules/Italy/Tele2_Internet.rst
	  (removed): now it works

2005-03-29 12:46 +0000 [r401591]  ogoffart

	* kopete/VERSION: Update Version BUG: 102722

2005-03-29 15:24 +0000 [r401642]  ogoffart

	* kopete/kopete/systemtray.cpp, kopete/kopete/systemtray.h:
	  Backport fix for Bug 100054

2005-03-29 17:16 +0000 [r401680]  ogoffart

	* kopete/kopete/contactlist/kopetecontactlistview.cpp,
	  kopete/kopete/contactlist/kopetecontactlistview.h: Backport the
	  fix for drag and drop temporary contacts

2005-03-30 22:58 +0000 [r402051]  ogoffart

	* kopete/libkopete/kopetepluginmanager.cpp: Backport the save bug
	  on exit (part1)

2005-03-30 23:01 +0000 [r402055]  ogoffart

	* kopete/kopete/kopeteapplication.cpp,
	  kopete/kopete/kopeteapplication.h: Backport saving contactlist at
	  exit (part2)

2005-03-31 08:29 +0000 [r402085]  ogoffart

	* kopete/kopete/contactlist/kopetemetacontactlvi.cpp: Backport fix
	  for Bug 101665: Contact photos wider than they are tall don't
	  keep their correct aspect

2005-03-31 10:24 +0000 [r402107]  ogoffart

	* kopete/kopete/contactlist/kopetemetacontactlvi.cpp,
	  kopete/kopete/contactlist/kopetegroupviewitem.cpp: Backport fix
	  for Bug 102288: When you change the system font sizes in
	  kcontrol, Kopete contact list does not pick them up until restart

2005-04-02 04:27 +0000 [r402573]  mattr

	* kopete/protocols/yahoo/yahooaccount.cpp: BACKPORT: Make sure to
	  not create a new session before we actually we want to connect.
	  Otherwise, we override the correct session that's already
	  connected, which is what leads to being able to receive but not
	  send messages. Should be in KDE 3.4.1 CCBUG: 102274

2005-04-02 04:35 +0000 [r402576]  mattr

	* kopete/protocols/oscar/oscaraccount.cpp: Backport the fix for
	  100064 CCBUG: 100064

2005-04-02 10:16 +0000 [r402601]  larkang

	* kget/kget_plug_in/kget_plug_in.cpp: Backport: Fix export

2005-04-02 12:24 +0000 [r402630]  swinter

	* wifi/interface_wireless_wirelessextensions.cpp: yet another
	  driver issue. There is at least one card+driver that report the
	  current signal quality is higher than the maximum possible
	  quality. This leads to values over 100 after normalisation. This
	  one-liner fixes that.

2005-04-02 13:45 +0000 [r402642]  ogoffart

	* kopete/protocols/sms/smsaccount.cpp: Backport Let send SMS
	  messages.

2005-04-02 22:50 +0000 [r402748]  mattr

	* kopete/protocols/oscar/aim/aimaccount.cpp,
	  kopete/protocols/oscar/icq/icqaccount.cpp: Backport the fix for
	  101705 for KDE 3.4.1 CCBUG: 101705

2005-04-03 05:27 +0000 [r402790]  mattr

	* kopete/protocols/oscar/liboscar/Makefile.am,
	  kopete/protocols/oscar/liboscar/oscarsettings.cpp,
	  kopete/protocols/oscar/liboscar/oscarsettings.h,
	  kopete/protocols/oscar/liboscar/client.cpp,
	  kopete/protocols/oscar/liboscar/connection.cpp,
	  kopete/protocols/oscar/liboscar/senddcinfotask.cpp,
	  kopete/protocols/oscar/liboscar/client.h,
	  kopete/protocols/oscar/liboscar/connection.h,
	  kopete/protocols/oscar/icq/icqaccount.cpp: Backport the enabling
	  of the web aware and hide ip settings. No new strings and adding
	  back functionality that was present in the previous released
	  version

2005-04-06 01:23 +0000 [r403437]  thiago

	* kopete/protocols/oscar/liboscar/oscarsettings.h: Backporting the
	  visibility fix. BACKPORT:1.1:1.2

2005-04-07 11:04 +0000 [r403739]  boiko

	* kopete/protocols/oscar/icq/icqaccount.cpp: Escape html tags in
	  incoming messages

2005-04-07 11:12 +0000 [r403742]  boiko

	* kopete/kopete/chatwindow/chatmessagepart.cpp: Do not load
	  external references

2005-04-07 11:21 +0000 [r403744]  ogoffart

	* kopete/libkopete/kopeteonlinestatusmanager.cpp: Backport the 85%
	  gray offline icon patch, because more people report the bug
	  103278

2005-04-07 12:03 +0000 [r403751]  boiko

	* kopete/protocols/oscar/icq/icqaccount.cpp: Using
	  Kopete::Message::escape() to escape the html tags. Thanks Gof for
	  the hint.

2005-04-07 13:25 +0000 [r403776]  ogoffart

	* kopete/protocols/yahoo/yahoocontact.cpp: Backport the fix in
	  YahooContact::sync

2005-04-07 17:06 +0000 [r403825]  tyfon

	* kopete/plugins/alias/aliaspreferences.cpp: Backport of a bugfix
	  for a bug that caused kopete to crash when adding aliases to a
	  protocol for which you did not have an account enabled. Ex. add
	  an alias to Yahoo when you only have IRC would crash.

2005-04-07 17:52 +0000 [r403837]  tyfon

	* kopete/plugins/alias/aliaspreferences.cpp: vim killed my tabs,
	  fix

2005-04-07 22:35 +0000 [r403923]  wstephens

	* kopete/protocols/groupwise/ui/gwcontactpropswidget.ui,
	  kopete/protocols/groupwise/gwcontact.cpp,
	  kopete/protocols/groupwise/gwaccount.cpp,
	  kopete/protocols/groupwise/gwcontactlist.h,
	  kopete/protocols/groupwise/gwcontact.h,
	  kopete/protocols/groupwise/gwaccount.h,
	  kopete/protocols/groupwise/gwprotocol.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/createcontacttask.cpp,
	  kopete/protocols/groupwise/gwprotocol.h,
	  kopete/protocols/groupwise/libgroupwise/tasks/movecontacttask.cpp,
	  kopete/protocols/groupwise/libgroupwise/gwerror.h,
	  kopete/protocols/groupwise/ui/gwprivacydialog.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/movecontacttask.h,
	  kopete/protocols/groupwise/ui/gwaddcontactpage.cpp,
	  kopete/protocols/groupwise/gwmessagemanager.cpp,
	  kopete/protocols/groupwise/ui/gwsearchwidget.ui,
	  kopete/protocols/groupwise/ui/gwsearch.cpp,
	  kopete/protocols/groupwise/libgroupwise/tasks/modifycontactlisttask.cpp,
	  kopete/protocols/groupwise/ui/gwreceiveinvitationdialog.cpp,
	  kopete/protocols/groupwise/ui/gwcontactproperties.cpp,
	  kopete/protocols/groupwise/ui/gwaccountpreferences.ui,
	  kopete/protocols/groupwise/gwcontactlist.cpp,
	  kopete/protocols/groupwise/ui/gwsearch.h,
	  kopete/protocols/groupwise/Makefile.am,
	  kopete/protocols/groupwise/ui/gwcontactproperties.h: BACKPORT:
	  Spring cleaning for GroupWise: * Better server side contact list
	  model fixing the crash sometimes adding contacts * Fix add
	  contacts in new folders * Better syncContact() for moving
	  contacts between folders, including new folders * Sync offline
	  changes to the server side contact list to the local list *
	  Cleaner search UI * Ability to copy user info to clipboard *
	  Email address, work phone and mobile phone are exposed as Kopete
	  Contact Properties * Fixed an incorrect string when blocking
	  contacts

2005-04-07 23:59 +0000 [r403935]  mattr

	* kopete/protocols/oscar/liboscar/coreprotocol.cpp,
	  kopete/protocols/oscar/liboscar/snacprotocol.cpp: Backport the
	  fix for the 'unknown wire format' bugs. The fix should be in KDE
	  3.4.1 CCBUGS: 101713, 101303, 102807

2005-04-08 10:03 +0000 [r404008-404006]  wstephens

	* kopete/protocols/gadu/gadueditaccount.cpp: Backport tab order fix

	* kopete/protocols/yahoo/yahooeditaccount.cpp,
	  kopete/protocols/irc/ui/irceditaccount.ui,
	  kopete/protocols/oscar/aim/ui/aimeditaccountwidget.cpp: Backport
	  taborder fix

	* kopete/protocols/jabber/ui/jabbereditaccountwidget.cpp,
	  kopete/protocols/oscar/icq/ui/icqeditaccountwidget.cpp: backport
	  taborder fix

2005-04-11 08:56 +0000 [r404733]  gianni

	* kppp/Rules/Italy/Tele2_Internet_Tele2.rst: change ruleset name

2005-04-11 11:26 +0000 [r404761]  adrian

	* kopete/libkopete/kopeteaway.h: fix compile with --enable-final
	  with gcc 4

2005-04-11 15:06 +0000 [r404817]  ogoffart

	* kopete/libkopete/kopeteaway.h: Fix compilation on gcc3

2005-04-11 17:24 +0000 [r404847]  ogoffart

	* kopete/protocols/msn/msnswitchboardsocket.cpp: Backport fix for
	  bug 102371

2005-04-12 14:28 +0000 [r405067]  adrian

	* kopete/libkopete/kopeteaway.h: yet another gcc 4 compilation fix,
	  oscar plugin didn't compiled anymore without KopeteAwayDialog
	  definition.

2005-04-12 21:44 +0000 [r405167]  porten

	* kppp/ChangeLog: improved byte number formatting patch by 'Walter'
	  FEATURE:103754

2005-04-13 07:55 +0000 [r405239]  porten

	* kppp/pppstatdlg.cpp, kppp/version.h: merged from/into head and
	  branch. previous commit mixed things up.

2005-04-13 19:00 +0000 [r405403]  bram

	* kopete/protocols/groupwise/ui/gwsearchwidget.ui: Backport:
	  fixuifiles

2005-04-13 21:09 +0000 [r405439]  gianni

	* wifi/kcmwifi/kcmwifi.cpp: fix a translation not available bug

2005-04-16 07:18 +0000 [r405844]  swinter

	* wifi/kcmwifi/kcmwifi.cpp: corrected two errors in the command
	  line of iwconfig: - indexes must count from [1] to [4], before
	  they were constantly setting keys in slot [1] which broke things
	  if more than one key was used - the key argument is seperated by
	  space and so it must be passed to iwconfig as a seperate input (I
	  think) WEP setting might still not work because iwconfig doesn't
	  seem to like the quite long command line it gets (it works from a
	  plain shell though, so maybe a KProcess problem?)

2005-04-16 07:42 +0000 [r405845]  swinter

	* wifi/kcmwifi/kcmwifi.cpp: split the huge iwconfig command in
	  parts. Before: if one part fails, the rest of the command was
	  ignored, which sucks. For KDE 3.5, it is now possible to report
	  back to the user which parts could not be executed (mostly
	  because of "Operation not supported") but still execute the other
	  parts.

2005-04-16 07:58 +0000 [r405846]  swinter

	* wifi/kcmwifi/wificonfig.cpp: fix that the "String?" property was
	  lost. This will NOT be upported to HEAD because there I will
	  implement an auto-detection (5 and 13 chars are string, 10 and 26
	  chars are hex, the rest is invalid)

2005-04-16 08:15 +0000 [r405848]  swinter

	* wifi/kcmwifi/wificonfig.cpp, wifi/kcmwifi/ifconfigpagebase.ui,
	  wifi/kcmwifi/wificonfig.h: introduce new bitrates for up to 54
	  MBit/s in the selection box if anyone knows the proprietary rates
	  above, please let me know

2005-04-16 11:41 +0000 [r405878]  swinter

	* wifi/kcmwifi/kcmwifi.cpp: no spaces in argv's

2005-04-18 09:39 +0000 [r406257]  lukas

	* kopete/kopete/contactlist/kabcexport.cpp: fix the unstranslatable
	  strings

2005-04-18 13:44 +0000 [r406308]  ogoffart

	* kopete/protocols/msn/msnp2p.cpp: backport fix for bug 97589 MSN
	  avatars exported by Kopete not shown in Gaim CCBUG: 97589

2005-04-18 14:50 +0000 [r406330]  ogoffart

	* kopete/kopete/config/behavior/behaviorconfig.cpp: Backport fix
	  for Bug 103889: behaviorconfig view plugin list entries multiply
	  on their own

2005-04-18 15:07 +0000 [r406338]  ogoffart

	* kopete/protocols/msn/msnmessagemanager.cpp: Backport fix for Bug
	  104040: Preference for timeout between AutoMessages for MSN
	  protocol has no effect

2005-04-18 15:20 +0000 [r406341]  ogoffart

	* kopete/protocols/oscar/oscarcontact.cpp: Backport fix for bug
	  102460 Fix 'online time' in tooltip for AIM/ICQ to show actual
	  online time of buddy, not your own

2005-04-18 16:34 +0000 [r406355]  ogoffart

	* kopete/protocols/msn/msnswitchboardsocket.cpp: Backport: don't
	  send a font name longer than 31 char

2005-04-19 20:17 +0000 [r406615]  ogoffart

	* kopete/protocols/msn/msnaccount.cpp: Backport fix for Bug 104214
	  : Reverse list does not work anymore in Kopete 0.10

2005-04-22 08:47 +0000 [r407094]  adridg

	* krdc/rdp/krdpview.cpp: According to rdesktop's help: rdesktop: A
	  Remote Desktop Protocol client. Version 1.4.0. Copyright (C)
	  1999-2005 Matt Chapman. See http://www.rdesktop.org/ for more
	  information. Usage: rdesktop [options] server[:port] The
	  server:port argument must come last.

2005-04-22 13:43 +0000 [r407143]  mattr

	* kopete/protocols/yahoo/libyahoo2/Makefile.am,
	  kopete/protocols/yahoo/libyahoo2/libyahoo2.c,
	  kopete/protocols/yahoo/libyahoo2/sha.c (removed),
	  kopete/protocols/yahoo/libyahoo2/sha1.c,
	  kopete/protocols/yahoo/libyahoo2/sha.h (removed),
	  kopete/protocols/yahoo/libyahoo2/sha1.h: backport the license
	  incompatibility fix

2005-04-22 22:48 +0000 [r407272]  mpyne

	* kfile-plugins/torrent/kfile_torrent.cpp,
	  kfile-plugins/torrent/bint.cpp, kfile-plugins/torrent/blist.cpp,
	  kfile-plugins/torrent/bytetape.cpp, kfile-plugins/torrent/bint.h:
	  Backport fix for bug 98942 (reported file length incorrect) to
	  KDE 3.4 CCBUG:98942

2005-04-23 13:55 +0000 [r407344]  mattr

	* kopete/protocols/oscar/liboscar/sendmessagetask.cpp: backport
	  autodeletion fix

2005-04-23 15:16 +0000 [r407364]  ogoffart

	* kopete/protocols/irc/ui/irceditaccount.ui: Backport fix for Bug
	  103238: Keyboard shortcut to 'Real name' [ALT+R] in IRC account
	  setup dialog/wizard not working, tab-order also wrong.

2005-04-24 11:26 +0000 [r407488]  ogoffart

	* kopete/protocols/msn/ui/msneditaccountwidget.cpp: Backport fix
	  for Bug 104445: MSN Display Picture Stretched

2005-04-25 22:14 +0000 [r407878]  mattr

	* kopete/libkopete/kopetenotifydataobject.h,
	  kopete/libkopete/Makefile.am,
	  kopete/libkopete/kopetenotifydataobject.cpp: backport
	  d-pointerization backport new installed headers

2005-04-27 11:23 +0000 [r408149]  ogoffart

	* kopete/protocols/msn/msnswitchboardsocket.cpp: Backport: don't
	  send typing notification if we are not fully connected to the
	  chat session

2005-04-27 12:09 +0000 [r408156]  ogoffart

	* kopete/libkopete/kopetepluginmanager.cpp: Backport: Make sure
	  PluginManager::shotdown() is called only once

2005-05-01 03:35 +0000 [r408936]  mattr

	* kopete/protocols/oscar/icq/ui/icqsearchdialog.cpp: clear the
	  search results before starting a new search. Backport from HEAD
	  Should be in KDE 3.4.1 BUG: 104373

2005-05-01 07:12 +0000 [r408950]  binner

	* filesharing/advanced/kcm_sambaconf/kcminterface.ui.h,
	  filesharing/advanced/kcm_sambaconf/kcmsambaconf.cpp: fix disabled
	  icons

2005-05-01 11:37 +0000 [r408996]  hermier

	* kopete/protocols/msn/msnnotifysocket.h,
	  kopete/protocols/msn/msnsocket.cpp,
	  kopete/protocols/msn/msnsocket.h,
	  kopete/protocols/msn/msnnotifysocket.cpp: CVS backport of the PNG
	  time out correction patch.

2005-05-01 13:07 +0000 [r409011]  ogoffart

	* kopete/protocols/msn/msnnotifysocket.cpp: Backport: ignore the
	  error 715

2005-05-06 12:23 +0000 [r410007]  binner

	* kget/icons/cr22-action-tool_delete.png (removed),
	  kget/kmainwidget.cpp, kget/transfer.cpp: Having a delete icon
	  which looks like trash is plain wrong warningContinueCancel* for
	  delete confirmations

2005-05-10 10:32 +0000 [r411876]  binner

	* kdenetwork.lsm: update lsm for release

2005-05-10 10:42 +0000 [r411884]  binner

	* kopete/kopete/main.cpp: Increasing versions listed in
	  RELEASE-CHECKLIST

2005-05-10 12:41 +0000 [r411915]  gianni

	* kppp/kpppwidget.cpp: i18n fix

2005-05-10 15:57 +0000 [r412025]  gerken

	* kopete/protocols/jabber/jabbergroupmembercontact.cpp,
	  kopete/protocols/jabber/Makefile.am,
	  kopete/protocols/jabber/jabberchatsession.cpp (added),
	  kopete/protocols/jabber/jabbermessagemanager.h (removed),
	  kopete/protocols/jabber/jabberchatsession.h (added),
	  kopete/protocols/jabber/jabbercontact.cpp,
	  kopete/protocols/jabber/jabbermessagemanager.cpp (removed):
	  Backport fixes from HEAD.

2005-05-10 16:07 +0000 [r412028]  gerken

	* kopete/protocols/jabber/jabberformtranslator.h,
	  kopete/protocols/jabber/jabberformtranslator.cpp: Backport fix
	  for #88226.

2005-05-10 16:48 +0000 [r412043]  gerken

	* kopete/protocols/jabber/jabberprotocol.cpp: Backport: make status
	  messages available again.

2005-05-10 16:52 +0000 [r412046]  qbast

	* kdnssd/ioslave/dnssd.cpp: I can't backport directly because it
	  would change string. This ugly hack removes tags after
	  translation. BUGS: 104809

2005-05-10 17:31 +0000 [r412061]  ogoffart

	* kopete/protocols/jabber/jabberprotocol.cpp: make it works with
	  libkopete from branch

2005-05-10 19:50 +0000 [r412128]  gerken

	* kopete/protocols/jabber/jabberprotocol.cpp,
	  kopete/protocols/jabber/jabberformtranslator.h,
	  kopete/protocols/jabber/jabberchatsession.cpp,
	  kopete/protocols/jabber/jabberchatsession.h,
	  kopete/protocols/jabber/jabbercontact.cpp,
	  kopete/protocols/jabber/jabberformtranslator.cpp: Revert previous
	  backports, they broke compilations and the changes are too
	  difficult to sort out.

2005-05-11 12:32 +0000 [r412328]  gerken

	* kopete/protocols/jabber/jabberformtranslator.cpp: Backport fix.

2005-05-11 14:07 +0000 [r412368]  ogoffart

	* kopete/plugins/latex/latexplugin.h,
	  kopete/plugins/latex/latexplugin.cpp: Backport: Security:
	  blacklist some latex command that may cause trouble. BUG: 103026

2005-05-11 15:21 +0000 [r412387]  gerken

	* kopete/protocols/jabber/jabberaccount.cpp: Backport #95248.

2005-05-11 16:40 +0000 [r412420]  ogoffart

	* kopete/plugins/latex/latexplugin.cpp: backport: don't blacklist
	  \iff

2005-05-11 19:47 +0000 [r412483]  ogoffart

	* kopete/protocols/msn/msnp2pincoming.cpp: Backport: make the
	  display picture working with MSN7 (i'm not sure this is required
	  to backport)

2005-05-13 04:39 +0000 [r413014]  mattr

	* kopete/protocols/yahoo/yahooaccount.cpp: Fix the big fonts people
	  are seeing in bug 100617. Patch by Andre Duffeck <andre at
	  duffeck dot de>. Thanks for the patch! Backported from HEAD,
	  should be in KDE 3.4.1 BUG: 100617

2005-05-13 07:46 +0000 [r413049]  markusb

	* kppp/devices.h: Reflect FreeBSD 6 sio driver device name change
	  cuaa -> cuad

2005-05-17 11:42 +0000 [r414940]  gerken

	* kopete/protocols/jabber/ui/dlgjabberbrowse.cpp,
	  kopete/protocols/jabber/ui/dlgbrowse.ui: Backport #88620.

2005-05-17 14:59 +0000 [r414998]  gerken

	* kopete/protocols/jabber/jabberformtranslator.h,
	  kopete/protocols/jabber/jabberformtranslator.cpp: Backport fix.

2005-05-17 19:52 +0000 [r415167]  swinter

	* wifi/asusled.cpp, wifi/status.cpp, wifi/kwifimanager.cpp:
	  backporting valgrind happiness

2005-05-17 21:38 +0000 [r415216]  gerken

	* kopete/protocols/jabber/jabberchatsession.cpp: Attempt a fix for
	  #99980 in branch.

2005-05-18 15:43 +0000 [r415435]  swinter

	* wifi/kcmwifi/mainconfig.cpp: backporting ignore wifiX devices on
	  autodetect

2005-05-18 20:31 +0000 [r415533]  dmacvicar

	* kopete/kopete/contactlist/kopetelviprops.cpp: backport: disable
	  sync photo if you are not using server photo

2005-05-18 22:25 +0000 [r415577-415576]  dmacvicar

	* kopete/plugins/latex/latexplugin.cpp: backport fix for bug
	  100134, fix scrolling

	* kopete/plugins/latex/latexplugin.cpp: fix backport fix for bug
	  100134, this include doesn't exist in branch

2005-05-19 10:33 +0000 [r415692]  ogoffart

	* kopete/protocols/msn/msnnotifysocket.cpp,
	  kopete/protocols/msn/msnnotifysocket.h,
	  kopete/protocols/msn/msnsocket.cpp,
	  kopete/protocols/msn/Makefile.am,
	  kopete/protocols/msn/sslloginhandler.cpp (added),
	  kopete/protocols/msn/sslloginhandler.h (added): Backport the msn
	  fix. hope that works

2005-05-19 14:39 +0000 [r415758]  ogoffart

	* kopete/protocols/msn/msnnotifysocket.cpp: compilation fix

2005-05-20 13:29 +0000 [r416060]  wstephens

	* kopete/protocols/msn/Makefile.am: SSLLoginHandler breaks
	  --enable-final because it uses the old style KInetSocketAddress
	  class and the rest of MSN uses the new
	  KNetwork::KInetSocketAddress.

2005-05-21 17:04 +0000 [r416475]  mattr

	* kopete/protocols/yahoo/yahooaccount.cpp: backport patch by Chetan
	  Reddy for yahoo custom status fixes. Fixes bug 98497. Hopefully,
	  it makes it in for KDE 3.4.1 BUG: 98497

2005-05-22 04:35 +0000 [r416635]  mattr

	* kopete/protocols/yahoo/libyahoo2/libyahoo2.c: backport 416633

2005-05-22 08:40 +0000 [r416666]  gerken

	* kopete/protocols/jabber/jabberaccount.cpp: Backport fix - free
	  file transfer port correctly.

2005-05-22 10:10 +0000 [r416699]  gianni

	* kppp/Rules/Italy/Tele2_Internet_Tele2.rst,
	  kppp/Rules/Italy/Tele2_Altri_ISP.rst: I confused 'comma' with
	  'dot'

2005-05-22 15:07 +0000 [r416917]  mattr

	* kopete/kopete/main.cpp: KDE 3.4.1 will have kopete 0.10.2 (which
	  has the MSN fix)

2005-05-22 15:27 +0000 [r416928]  gerken

	* kopete/protocols/jabber/ui/dlgjabbervcard.cpp: Backport fix for
	  #105110.

2005-05-22 15:38 +0000 [r416938]  gerken

	* kopete/protocols/jabber/jabbercontact.cpp: Backport fix for URL
	  display.

2005-05-22 15:44 +0000 [r416941]  coolo

	* ksirc/servercontroller.h,
	  kopete/libkopete/kopetemessagemanager.cpp,
	  kopete/libkopete/kopetemessagemanager.h: fix compile with gcc
	  4.0.1

2005-05-22 15:55 +0000 [r416947]  gerken

	* kopete/protocols/jabber/jabbercontact.cpp: Backporting cosmetic
	  fix.

2005-05-22 16:08 +0000 [r416955]  gerken

	* kopete/protocols/jabber/jabbercontact.cpp: Backport URL fix.

