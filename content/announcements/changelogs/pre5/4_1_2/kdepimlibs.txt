------------------------------------------------------------------------
r854917 | krake | 2008-08-30 17:48:30 +0200 (Sat, 30 Aug 2008) | 3 lines

Flag value should not be overlapping.
0x16 == 0x10 & 0x04 & 0x2

------------------------------------------------------------------------
r855227 | tmcguire | 2008-08-31 12:38:45 +0200 (Sun, 31 Aug 2008) | 16 lines

Backport r855106 by tmcguire from trunk to the 4.1 branch:

Merged revisions 852197 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r852197 | ervin | 2008-08-25 16:08:35 +0200 (Mon, 25 Aug 2008) | 6 lines
  
  Address the test case provided by Thomas for email address encoding. Too
  much quoting kills quoting apparently, so try to be a bit less
  aggressive when we normalize.
  
  CCMAIL: mcguire@kde.org
........


------------------------------------------------------------------------
r855228 | tmcguire | 2008-08-31 12:39:57 +0200 (Sun, 31 Aug 2008) | 24 lines

Backport r855109 by tmcguire from trunk to the 4.1 branch:

Merged revisions 854349 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

................
  r854349 | vkrause | 2008-08-29 14:41:09 +0200 (Fri, 29 Aug 2008) | 14 lines
  
  Merged revisions 840650 via svnmerge from 
  https://vkrause@svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r840650 | vkrause | 2008-08-01 14:15:29 +0200 (Fri, 01 Aug 2008) | 7 lines
    
    Save a few more percent on agenda item generation in KOrganizer by
    avoiding duplicate call to the rather expensive dtXTimeStr() methods.
    However, the real fix is probably to create the tooltip texts on demand
    rather all of them while creating the agenda items.
    
    Kolab issue 2904
  ........
................


------------------------------------------------------------------------
r855229 | tmcguire | 2008-08-31 12:41:17 +0200 (Sun, 31 Aug 2008) | 12 lines

Backport r855110 by tmcguire from trunk to the 4.1 branch:

Merged revisions 854627 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r854627 | osterfeld | 2008-08-30 00:21:50 +0200 (Sat, 30 Aug 2008) | 1 line
  
  use LIBS, not LIBRARY, needed to get libkemoticons
........


------------------------------------------------------------------------
r856768 | tmcguire | 2008-09-03 20:33:54 +0200 (Wed, 03 Sep 2008) | 6 lines

Backport r855774 by ogoffart from trunk to the 4.1 branch:

Fix crash (while displaying a mail which has a calendar entry in it)



------------------------------------------------------------------------
r859254 | winterz | 2008-09-09 23:04:22 +0200 (Tue, 09 Sep 2008) | 11 lines

backport SVN commit 859249 by winterz:

Enterprise4 Merge
SVN commit 859216 by vkrause:

Update the incidence for date hashs on changes correctly.
This fixes various strange effects in the KOrganizer agenda view after moving
an event or adding recurrence, such as duplicate, jumping or entirely vanished
entries.


------------------------------------------------------------------------
r859481 | winterz | 2008-09-10 14:51:18 +0200 (Wed, 10 Sep 2008) | 8 lines

backport SVN commit 859346 and 859347 by tstaerk:

The time-shift should take place in incidenceformatter, not in
icalformat. Thanks David for the review.

Make sure msg exists. Why do those ideas come after committing? Because a human mind is krep.


------------------------------------------------------------------------
r859713 | krake | 2008-09-11 09:03:59 +0200 (Thu, 11 Sep 2008) | 7 lines

Backport of revision 859712

Always initialize members.

Fixes an update notification issue, i.e. updates on cloned incidences weren't reported correctly since the mUpdateGroupLevel value was uninitialized and never reached 0 in endUpdates()


------------------------------------------------------------------------
r859717 | krake | 2008-09-11 09:13:06 +0200 (Thu, 11 Sep 2008) | 5 lines

Backport of revision 859714

Always check for self assignment in operator=(), especially when clearing things prior to copying or when juggling pointers


------------------------------------------------------------------------
r861673 | winterz | 2008-09-16 21:57:00 +0200 (Tue, 16 Sep 2008) | 6 lines

backport SVN commit 861668 by vkrause:

Suppress update notification for incidences that are about to be
deleted. Those can be triggered by removing inter-incidence relations
and cause a crash.

------------------------------------------------------------------------
r862121 | scripty | 2008-09-18 08:21:57 +0200 (Thu, 18 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r862510 | scripty | 2008-09-19 08:01:53 +0200 (Fri, 19 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r863249 | lueck | 2008-09-21 17:26:28 +0200 (Sun, 21 Sep 2008) | 2 lines

fix porting error caused by the module reorganisation. + 52 new messages
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r864184 | scripty | 2008-09-24 09:03:25 +0200 (Wed, 24 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
