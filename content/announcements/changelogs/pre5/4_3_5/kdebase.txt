------------------------------------------------------------------------
r1055428 | dfaure | 2009-11-28 00:55:46 +0000 (Sat, 28 Nov 2009) | 4 lines

Don't move the home directory just because some path used to point to it. Don't even offer it ;)
Fixed for: 4.3.4
CCBUG: 193057

------------------------------------------------------------------------
r1055435 | mpyne | 2009-11-28 01:32:47 +0000 (Sat, 28 Nov 2009) | 2 lines

Backport correct comment style fix for kio_info to KDE 4.3.4.

------------------------------------------------------------------------
r1055890 | cfeck | 2009-11-28 23:13:44 +0000 (Sat, 28 Nov 2009) | 4 lines

Handle failure of usb_open() (backport r1055888)

CCBUG: 206566

------------------------------------------------------------------------
r1055928 | scripty | 2009-11-29 04:14:04 +0000 (Sun, 29 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1057349 | scripty | 2009-12-02 04:16:43 +0000 (Wed, 02 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1057674 | scripty | 2009-12-03 04:11:54 +0000 (Thu, 03 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1059124 | hindenburg | 2009-12-05 21:11:32 +0000 (Sat, 05 Dec 2009) | 4 lines

Correct terminal focus issues when clicking on tabbar.

CCBUG: 215382

------------------------------------------------------------------------
r1060091 | cfeck | 2009-12-08 02:04:11 +0000 (Tue, 08 Dec 2009) | 6 lines

Fix crashes in handleRandREvent() with invalid crtc/output (backport r1060090)

CCBUG: 196407
CCBUG: 209943
CCBUG: 217807

------------------------------------------------------------------------
r1060130 | wstephens | 2009-12-08 08:49:00 +0000 (Tue, 08 Dec 2009) | 3 lines

Backport r1060085 to KDE 4.3 branch
CCBUG: 212810

------------------------------------------------------------------------
r1060485 | scripty | 2009-12-09 05:50:54 +0000 (Wed, 09 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1060614 | darioandres | 2009-12-09 11:40:07 +0000 (Wed, 09 Dec 2009) | 8 lines

Backport of:
SVN commit 1060612 by darioandres:

- setParent() was not needed and could cause a crash
  Thanks Christoph for checking it

CCBUG: 217935

------------------------------------------------------------------------
r1060848 | scripty | 2009-12-10 04:16:01 +0000 (Thu, 10 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1060856 | scao | 2009-12-10 06:02:34 +0000 (Thu, 10 Dec 2009) | 1 line

this function should override superclass timerTick
------------------------------------------------------------------------
r1061796 | scripty | 2009-12-13 04:03:45 +0000 (Sun, 13 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1062215 | jacopods | 2009-12-14 08:50:10 +0000 (Mon, 14 Dec 2009) | 3 lines

backport 1062214
CCBUG:205632

------------------------------------------------------------------------
r1062219 | jacopods | 2009-12-14 09:21:50 +0000 (Mon, 14 Dec 2009) | 2 lines

argh, partially revert previous commit 1062215, as it contained another unrelated patch; 

------------------------------------------------------------------------
r1062220 | jacopods | 2009-12-14 09:29:11 +0000 (Mon, 14 Dec 2009) | 2 lines

Fix effectframe corruption when toggling compositing

------------------------------------------------------------------------
r1062223 | johnflux | 2009-12-14 09:34:02 +0000 (Mon, 14 Dec 2009) | 2 lines

Backport - discard old data

------------------------------------------------------------------------
r1062413 | adawit | 2009-12-14 17:30:58 +0000 (Mon, 14 Dec 2009) | 1 line

Backport the fix for the Y2K38 problem with the cookiejar's expiration date variable
------------------------------------------------------------------------
r1062543 | scripty | 2009-12-15 04:02:39 +0000 (Tue, 15 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1062799 | scripty | 2009-12-16 04:09:41 +0000 (Wed, 16 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1063660 | segato | 2009-12-19 00:49:11 +0000 (Sat, 19 Dec 2009) | 4 lines

backport r1063658
add kde to the user startmenu instead of the global one, this fixes the problem that the startmenu entries are 
not generated when uac is enabled

------------------------------------------------------------------------
r1064069 | scripty | 2009-12-20 04:11:19 +0000 (Sun, 20 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1064495 | scripty | 2009-12-21 04:21:58 +0000 (Mon, 21 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1067974 | scripty | 2009-12-31 04:14:04 +0000 (Thu, 31 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1068366 | scripty | 2010-01-01 04:11:22 +0000 (Fri, 01 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1068597 | lunakl | 2010-01-01 17:05:40 +0000 (Fri, 01 Jan 2010) | 5 lines

backport r1068595
Set WINDOW_ROLE so that KWin's window-specific settings can detect
different panels.


------------------------------------------------------------------------
r1068599 | lunakl | 2010-01-01 17:13:35 +0000 (Fri, 01 Jan 2010) | 5 lines

backport r1017015
filled titlePix with transparent color before painting pattern, to fix decoration appearance
BUG: 210540


------------------------------------------------------------------------
r1068820 | scripty | 2010-01-02 03:58:13 +0000 (Sat, 02 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1069209 | lunakl | 2010-01-02 23:03:29 +0000 (Sat, 02 Jan 2010) | 5 lines

Backport r1069208.
Do not show a new window too soon, as it messes up handling
of the MMB-on-a-link-opens-new-window-in-background feature.


------------------------------------------------------------------------
r1071180 | adawit | 2010-01-07 17:39:17 +0000 (Thu, 07 Jan 2010) | 1 line

Adjusted the webkit part support
------------------------------------------------------------------------
r1072049 | graesslin | 2010-01-09 10:42:35 +0000 (Sat, 09 Jan 2010) | 4 lines

Backport rev 1072044
When there is no window, we can't select one with a mouse click in coverswitch.
CCBUG: 221626

------------------------------------------------------------------------
r1072374 | scripty | 2010-01-10 04:08:29 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1072713 | graesslin | 2010-01-10 19:38:37 +0000 (Sun, 10 Jan 2010) | 4 lines

Backport rev 1072707
Use kwin icon in the killer helper window.
CCBUG: 210172

------------------------------------------------------------------------
r1072829 | scripty | 2010-01-11 04:18:09 +0000 (Mon, 11 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1073251 | graesslin | 2010-01-11 21:29:35 +0000 (Mon, 11 Jan 2010) | 4 lines

Backport rev 1073247
Do not add NULL effect windows to the motion manager.
CCBUG: 222193

------------------------------------------------------------------------
r1073383 | scripty | 2010-01-12 04:25:26 +0000 (Tue, 12 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1074947 | kossebau | 2010-01-15 01:13:36 +0000 (Fri, 15 Jan 2010) | 2 lines

backport of #1074945: fixed: update parameter values of --geticon to that of KDE4

------------------------------------------------------------------------
r1074958 | kossebau | 2010-01-15 01:50:52 +0000 (Fri, 15 Jan 2010) | 2 lines

backport of 1074955: fixed: KIconLoader::FileSystem is deprecated, use KIconLoader::Place

------------------------------------------------------------------------
r1074982 | scripty | 2010-01-15 04:01:41 +0000 (Fri, 15 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1075069 | pino | 2010-01-15 14:06:38 +0000 (Fri, 15 Jan 2010) | 2 lines

simplify construction

------------------------------------------------------------------------
r1075901 | aacid | 2010-01-16 23:32:20 +0000 (Sat, 16 Jan 2010) | 7 lines

Backport r1062086 | aacid | 2009-12-13 20:00:39 +0100 (dg, 13 des 2009) | 3 lines
Changed paths:
   M /trunk/KDE/kdebase/runtime/kpasswdserver/kpasswdserver.cpp

only check the anon field if no user/pass is given
acked by lemma

------------------------------------------------------------------------
r1076115 | freininghaus | 2010-01-17 15:00:05 +0000 (Sun, 17 Jan 2010) | 22 lines

Backport commits 1060716 and 1062076 to the 4.3 branch:

Rename DolphinDetailsView's nameColumnRect member to visualRect.

This overrides QTreeView::visualRect, such that the "visual rect"
matches the area used in indexAt. Fixes the problem that the selection
loses items in the Details View when selecting new items with
Shift+Keyboard (if Qt 4.6 is used).

CCBUG: 217447

Reimplement visualRegionForSelection in DolpinDetailsView.

Fixes the problem that not the entire area affected by changing the
selection gets updated. QTreeView::visualRegionForSelection assumes
implicitly that the visualRects of all items have the same width,
which is not the case here after the fix for bug 217447.

CCBUG: 218114

Fixes will be in KDE 4.3.5.

------------------------------------------------------------------------
r1076366 | scripty | 2010-01-18 04:08:22 +0000 (Mon, 18 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1076569 | trueg | 2010-01-18 11:31:52 +0000 (Mon, 18 Jan 2010) | 4 lines

Backport: fix crash for nested analysis results in addTriplet.

BUG: 220791

------------------------------------------------------------------------
r1076767 | mueller | 2010-01-18 21:07:44 +0000 (Mon, 18 Jan 2010) | 2 lines

bump version to 4.3.5

------------------------------------------------------------------------
r1076800 | rjarosz | 2010-01-18 21:50:02 +0000 (Mon, 18 Jan 2010) | 10 lines

Backport commit 1076786.

If notification settings have changed reparse cached configs, clearing the cache may not work because there is another "cache" in KSharedConfig::Ptr and if some object holds the KSharedConfig::Ptr the configuration may not get reparsed at all (and it happens because there is a leak in plasma notifications).

CCBUG: 203297
CCBUG: 217759
CCBUG: 201909
CCBUG: 189120


------------------------------------------------------------------------
r1076930 | scripty | 2010-01-19 04:21:29 +0000 (Tue, 19 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1077227 | esben | 2010-01-19 18:39:48 +0000 (Tue, 19 Jan 2010) | 3 lines

Reimplement lost automatic sizing function for popup menu in Klipper 
CCBUG: 170278

------------------------------------------------------------------------
r1077232 | esben | 2010-01-19 18:50:16 +0000 (Tue, 19 Jan 2010) | 3 lines

Clear on-disk clipboard history when either the user switches from saving history to not, or when manually clearing history 
CCBUG: 142882

------------------------------------------------------------------------
r1077810 | jacopods | 2010-01-20 23:05:51 +0000 (Wed, 20 Jan 2010) | 3 lines

backport to 4.3; will be in 4.3.5
CCBUG: 199325

------------------------------------------------------------------------
r1078178 | dfaure | 2010-01-21 17:59:17 +0000 (Thu, 21 Jan 2010) | 5 lines

Backport r1075552 (Fix kded using 100% CPU) using QPointer instead of QWeakPointer
(thanks to Rex Dieter for the converted patch)
Fixed for: 4.3.5
CCBUG: 202744

------------------------------------------------------------------------
r1078185 | dfaure | 2010-01-21 18:18:03 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport 1057148: Fixed statusbar rendering bug (multiple views visible) when restoring multiple tabs.
Fixed for: 4.3.5
CCBUG: 169124, 158900

------------------------------------------------------------------------
r1078191 | dfaure | 2010-01-21 18:27:07 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport 1050276: Repair 'right click goes back in history' feature.
Fixed for: 4.3.5
CCBUG: 168439

------------------------------------------------------------------------
r1078192 | dfaure | 2010-01-21 18:28:05 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport 1076829: Fix crash on Ctrl+Tab.
Fixed for: 4.3.5
CCBUG: 203809

------------------------------------------------------------------------
r1078193 | dfaure | 2010-01-21 18:29:58 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport r1065134: Repair 'Send File' so that it only sends the selected files, like in kde3.
Fixed for: 4.3.5
CCBUG: 218388

------------------------------------------------------------------------
r1078194 | dfaure | 2010-01-21 18:32:19 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport r1050729: Save/restore the expanded/collapsed state of bookmark folders.
Fixed for: 4.3.5
CCBUG: 131127

------------------------------------------------------------------------
