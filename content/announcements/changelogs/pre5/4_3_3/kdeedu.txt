------------------------------------------------------------------------
r1032032 | aacid | 2009-10-06 17:33:29 +0000 (Tue, 06 Oct 2009) | 3 lines

Fix switch between mexico state and the federal district
BUGS: 209602

------------------------------------------------------------------------
r1032036 | asimha | 2009-10-06 17:38:24 +0000 (Tue, 06 Oct 2009) | 6 lines

Backporting fix to bug #209646

CCMAIL: kstars-devel@kde.org
CCBUG: 209646


------------------------------------------------------------------------
r1032051 | asimha | 2009-10-06 18:15:19 +0000 (Tue, 06 Oct 2009) | 5 lines

Backporting r1032048 to 4.3

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r1032767 | annma | 2009-10-08 13:13:23 +0000 (Thu, 08 Oct 2009) | 3 lines

backport backspace fix
BUG=209821

------------------------------------------------------------------------
r1036159 | rahn | 2009-10-16 18:10:28 +0000 (Fri, 16 Oct 2009) | 1 line

- Support systems with a Qt version that doesn't have printer support compiled in.
------------------------------------------------------------------------
r1036540 | rahn | 2009-10-17 11:47:03 +0000 (Sat, 17 Oct 2009) | 4 lines

- dashed and dotted lines are quite slow on some plattforms so we avoid
  them in Low Quality mode for country borders


------------------------------------------------------------------------
r1036617 | rahn | 2009-10-17 13:43:08 +0000 (Sat, 17 Oct 2009) | 3 lines

- Make coordinate grid more coarse and faster


------------------------------------------------------------------------
r1039909 | asimha | 2009-10-24 22:04:51 +0000 (Sat, 24 Oct 2009) | 13 lines

Backporting fix in Detail dialog (revisions 1039902 and 1039907) to
4.3 branch.

Original commit log:

Use the signal currentItemChanged instead of itemClicked to monitor
changes to the list of links in the detail dialog. This makes sure
that the correct link is opened even when the user uses the keyboard
to navigate through the list of links.

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r1039932 | asimha | 2009-10-24 22:56:23 +0000 (Sat, 24 Oct 2009) | 6 lines

Backporting fixes to the details dialog to 4.3
(revisions 1039917, 1039918, and 1039925)

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r1041759 | lueck | 2009-10-28 13:09:46 +0000 (Wed, 28 Oct 2009) | 2 lines

layout fix for translated labels longer than the english text, this fix is already in the avogadro repo on sf.net
BUG:209872
------------------------------------------------------------------------
