2005-05-27 14:03 +0000 [r418718]  dgp

	* kuser/configure.in.in: Removed warning on sys/mount.h check on
	  FreeBSD.

2005-06-01 15:27 +0000 [r420860]  dfaure

	* kpackage/Makefile.am, kpackage/mimetypes (removed),
	  kpackage/kpackage.desktop: Don't provide your own mimetype, when
	  kdelibs provides it already, with a different name... BUG: 106607

2005-06-01 23:19 +0000 [r421008]  dfaure

	* kfile-plugins/deb/kfile_deb.cpp: Fix crash and use correct
	  mimetype. Rumours say this doesn't completely fix this plugin
	  though.

2005-06-27 15:18 +0000 [r429401]  binner

	* kdeadmin.lsm: 3.4.2 preparations

2005-06-28 14:47 +0000 [r429682]  toivo

	* kpackage/fbsdInterface.cpp: BSD fixes

2005-06-30 23:11 +0000 [r430330]  larkang

	* kpackage/gentooInterface.cpp: Backport: Install/Uninstall correct
	  version

2005-07-07 14:47 +0000 [r432486]  toivo

	* kpackage/fbsdInterface.cpp: Fix summary

