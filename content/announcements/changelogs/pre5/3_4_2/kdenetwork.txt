2005-05-23 22:44 +0000 [r417581]  gerken

	* kopete/protocols/jabber/jabberaccount.cpp: Backport better error
	  handling.

2005-05-24 16:00 +0000 [r417776]  dgp

	* configure.in.in: Cleaned up headers checks to avoid warnings
	  during ppp headers checks (both Linux and FreeBSD).

2005-05-25 03:43 +0000 [r417960]  mattr

	* kopete/protocols/oscar/liboscar/icquserinfo.cpp: backport the fix
	  for bug 106228. Should be in KDE 3.4.2 CCBUG: 106228

2005-05-26 06:22 +0000 [r418294]  gj

	* kopete/protocols/gadu/gaduaccount.cpp: Backport fix for 89124
	  BUG:89124

2005-05-28 12:40 +0000 [r419053]  jritzerfeld

	* kopete/protocols/oscar/icq/ui/icquserinfowidget.cpp,
	  kopete/protocols/oscar/liboscar/icquserinfo.cpp: Backport fix for
	  Bug 102991 (icq user info birthday sometimes wrong). BUG: 102991

2005-05-29 13:00 +0000 [r419439]  gerken

	* kopete/protocols/jabber/jabberaccount.cpp: Backport fix for
	  #92391. (display group chat error messages)

2005-05-29 14:27 +0000 [r419482]  ogoffart

	* kopete/libkopete/kopetepassword.cpp: Backport fix for Bug 106460:
	  Partial account data remains in kopeterc after account deletion

2005-05-29 14:48 +0000 [r419487]  gerken

	* kopete/protocols/jabber/jabberbasecontact.cpp: Format timestamps
	  correctly.

2005-05-30 14:18 +0000 [r419739]  gerken

	* kopete/protocols/jabber/jabberaccount.cpp: Fix my previous
	  string-freeze violation by using an existing string from
	  Groupwise.

2005-05-30 14:29 +0000 [r419746]  gerken

	* kopete/protocols/jabber/jabbercontact.h,
	  kopete/protocols/jabber/jabbergroupmembercontact.cpp,
	  kopete/protocols/jabber/ui/dlgjabbervcard.cpp,
	  kopete/protocols/jabber/jabbergroupmembercontact.h,
	  kopete/protocols/jabber/jabberbasecontact.cpp,
	  kopete/protocols/jabber/jabbergroupcontact.cpp,
	  kopete/protocols/jabber/jabberbasecontact.h,
	  kopete/protocols/jabber/jabbercontact.cpp,
	  kopete/protocols/jabber/jabbergroupcontact.h: Remove rename()
	  method from the Jabber contact classes and use the nick name
	  property appropriately. This makes synch() the only place for
	  nickname updates. Display names are now entirely handled by
	  Kopete. Since other protocols do not suffer from renaming loops,
	  this should fix bug #103716. BUG: 103716

2005-05-30 15:35 +0000 [r419764]  gerken

	* kopete/protocols/jabber/jabberaccount.cpp: String fix.

2005-05-30 15:55 +0000 [r419774]  gerken

	* kopete/protocols/jabber/jabbercontact.cpp: Don't display error
	  messages when trying to synch while being offline. (backport)

2005-05-30 18:46 +0000 [r419853-419851]  gerken

	* kopete/protocols/jabber/ui/dlgjabbervcard.cpp,
	  kopete/protocols/jabber/jabbercontact.cpp: Fix contact naming in
	  branch.

	* kopete/protocols/jabber/jabberbasecontact.cpp: Second part of
	  name handling corrections in branch.

2005-06-01 08:42 +0000 [r420726]  ogoffart

	* kopete/libkopete/kopeteprotocol.cpp: Backport the revision 408766
	  if kopeteprotocol.cpp "Don't let the myself contact be
	  deserialized, or it will conflict with the myself metacontact"
	  Well, there is no myself metacontact in branch, but it may
	  conflict anyway with all the myself contacts. This should fix Bug
	  102735: Kopete segmentation fault on unloading MSN plugin BUG:
	  102735

2005-06-01 09:32 +0000 [r420746]  ogoffart

	* kopete/protocols/msn/msnaccount.cpp: Backport ignore the myself
	  contact

2005-06-01 10:34 +0000 [r420765]  ogoffart

	* kopete/protocols/msn/msnp2pincoming.cpp: Backport, make sure this
	  is the preparation message

2005-06-01 12:02 +0000 [r420783]  gerken

	* kopete/libkopete/ui/kopetepasswordwidget.cpp: Make sure passwords
	  are being set, not concatenated. (backport)

2005-06-03 00:17 +0000 [r421398]  mattr

	* kopete/protocols/oscar/liboscar/offlinemessagestask.cpp: fix
	  solaris compile error in the branch as well. Should be in KDE
	  3.4.2 and later BUG: 99540

2005-06-04 13:44 +0000 [r422087]  pino

	* kget/docking.cpp, kget/icons/cr22-action-dock.png (removed),
	  kget/icons/cr22-action-kget_dock.png (added): Giving a better
	  icon for the 'dock' icon, so it's easily themeable by our
	  artists. BUG: 69207

2005-06-04 13:48 +0000 [r422088]  pino

	* kget, kget/kget_plug_in: Some ignores.

2005-06-05 09:15 +0000 [r422364]  ogoffart

	* kopete/plugins/latex/latexplugin.cpp: Backport Fixes: - Don't
	  show latex image if there is no image. - don't put <br/> in
	  tooltip - don't loose the message formating if any

2005-06-06 03:12 +0000 [r422661]  gj

	* kopete/protocols/gadu/gadudccserver.cpp: don't leak, backported

2005-06-06 03:20 +0000 [r422663]  gj

	* kopete/protocols/gadu/gadusession.cpp: don't leak here too

2005-06-08 18:51 +0000 [r423519]  mlarouche

	* kopete/libkopete/kopetemetacontact.cpp: BUG: Backport Metacontact
	  photo attributes was not saved when the metacontact has a KDE
	  Address Book link.

2005-06-09 17:13 +0000 [r423782]  bram

	* kopete/protocols/jabber/ui/dlgbrowse.ui: fixuifiles

2005-06-09 17:20 +0000 [r423784]  rantala

	* kopete/protocols/irc/kcodecaction.cpp: Backport: Fix a small bug
	  when changing the encoding for a channel or private chat via
	  IRC->Encoding menu item: The encoding changes, but the menu is
	  not updated to reflect that change, the 'tick' just stays in the
	  default selection.

2005-06-11 10:28 +0000 [r424273]  rantala

	* kopete/protocols/irc/ircaccount.cpp: backport fix for 104857 Go
	  online with IRC plugin and then go Away. If you then select
	  Online from the plugin toolbar icon menu, it does not do
	  anything.

2005-06-12 07:23 +0000 [r424513]  rantala

	* kopete/protocols/irc/ui/irceditaccount.ui: Change checkbox name
	  'Exclude from connection' -> 'Exclude from connect all' and
	  provide whatsThis. Both new strings are 1:1 copy from MSN plugin.

2005-06-12 20:24 +0000 [r424713]  mattr

	* kopete/kopete/chatwindow/chatview.cpp,
	  kopete/kopete/chatwindow/chatview.h: backport the showing of the
	  auto spell check status in the status bar. uses an existing
	  string from kdelibs.

2005-06-13 10:59 +0000 [r424865]  ogoffart

	* kopete/protocols/msn/msnswitchboardsocket.cpp: Backport: Make
	  sure the message are shown in the chatwindow even if a custom
	  emoticon transfer failed. BUG: 104210 The fix will be in KDE
	  3.4.2

2005-06-13 22:25 +0000 [r425129]  dmacvicar

	* kopete/libkopete/kopetemetacontact.cpp: backport of bug that
	  overwrites your entire addressbook with lame msn pictures when a
	  resource of kabc is not available.

2005-06-14 12:32 +0000 [r425312]  ogoffart

	* kopete/protocols/msn/msnp2pdisplatcher.cpp: Backport: better
	  error handling

2005-06-16 15:59 +0000 [r426150]  rantala

	* kopete/plugins/alias/aliaspreferences.cpp: backport fix for
	  105153 This correction fixes the hangs I was able to reproduce.
	  It was caused by a while() loop in the destructor where someone
	  forgot to advance an iterator.

2005-06-16 19:43 +0000 [r426254]  rantala

	* kopete/protocols/irc/ircusercontact.cpp: backport: If a user is
	  away, the server returns something like 312 mynick othernick
	  localhost.localdomain :FooNet Server but the protocol code ends
	  up calling setProperty(m_protocol->propLastSeen,
	  QDateTime::fromString("FooNet Server"))

2005-06-16 20:14 +0000 [r426267]  ogoffart

	* kopete/protocols/msn/msnaccount.cpp: Backport: typo

2005-06-17 12:34 +0000 [r426470]  rantala

	* kopete/plugins/alias/aliaspreferences.cpp: backport: Another
	  iterator related bug. This caused some hangs too.

2005-06-19 21:09 +0000 [r427166]  jritzerfeld

	* kopete/protocols/oscar/icq/ui/icqeditaccountui.ui: Backport fix
	  for Bug 107444 (connect from toolbar icon fails). CCBUG: 107444

2005-06-19 22:02 +0000 [r427188]  ogoffart

	* kopete/protocols/msn/msnnotifysocket.cpp,
	  kopete/protocols/msn/msnnotifysocket.h,
	  kopete/protocols/msn/msnfiletransfersocket.cpp: Use the correct
	  IP for outgoing file transfer. This allow to send file over a nat
	  ro router, but still require to forward the port 6891 this help
	  for Bug 57931 (Define Hostname/IP for File Transfers) and Bug
	  96205 (kopete is unable to send files via msn behind a nat
	  router) BUG: 57931 BUG: 96205

2005-06-20 09:10 +0000 [r427299]  rantala

	* kopete/plugins/webpresence/webpresenceplugin.cpp,
	  kopete/plugins/webpresence/webpresenceplugin.h: backport: Change
	  from QPtrList<Kopete::Protocol> to typedef
	  QValueList<Kopete::Protocol*> ProtocolList;

2005-06-23 11:03 +0000 [r428163]  swinter

	* wifi/interface_wireless_wirelessextensions.cpp: compile with
	  wireless extensions v28 patch from Michael Olbrich (backported
	  from trunk) CCMAIL:michael.olbrich@gmx.net

2005-06-24 00:49 +0000 [r428405]  mattr

	* kopete/protocols/groupwise/ui/gwcontactpropswidget.ui,
	  kopete/kopete/chatwindow/kopetechatwindow.cpp: compile fixes for
	  KDE 3.3

2005-06-26 09:49 +0000 [r429048]  ogoffart

	* kopete/plugins/cryptography/cryptographyguiclient.h,
	  kopete/plugins/cryptography/cryptographyguiclient.cpp: backport:
	  crash when toolge encryption of a contact that has changed
	  metacontact. CCBUG: 108155

2005-06-26 14:23 +0000 [r429090]  mattr

	* kopete/protocols/oscar/oscarcontact.cpp: don't send escaped text.
	  backport of 102880

2005-06-26 22:16 +0000 [r429198]  jritzerfeld

	* kopete/protocols/oscar/aim/ui/aimeditaccountui.ui: Backport AIM
	  fix for Bug 107444 (connect from toolbar icon fails). CCBUG:
	  107444

2005-06-27 15:18 +0000 [r429401]  binner

	* kdenetwork.lsm: 3.4.2 preparations

2005-06-27 15:38 +0000 [r429402]  binner

	* kopete/kopete/main.cpp: more RELEASE-CHECKLIST fun

2005-07-01 19:52 +0000 [r430561]  porten

	* kppp/ChangeLog, kppp/connect.cpp: integrated i18n fix for flow
	  control

2005-07-01 22:34 +0000 [r430606]  porten

	* kppp/general.cpp, kppp/connect.cpp, kppp/version.h: merged
	  further flow control logic fixes

2005-07-05 03:31 +0000 [r431714]  jlee

	* krdc/krdc.cpp: BUG: 95609 Backported bug fix. Window title now
	  displays hostname properly.

2005-07-05 21:45 +0000 [r432026]  hermier

	* kopete/protocols/irc/libkirc/kircmessage.h,
	  kopete/protocols/irc/libkirc/kircmessage.cpp: Backport commit
	  #432024:Lets squash some ennoying encoding bugs.

2005-07-05 22:27 +0000 [r432032]  hermier

	* kopete/protocols/irc/libkirc/kircengine_numericreplies.cpp: Don't
	  annoy the user on registered nick. Due to the online detection
	  this message was displayed every 30sec on the chat log, Ignore it
	  for now.

2005-07-06 19:51 +0000 [r432251]  hermier

	* kopete/protocols/irc/irccontact.cpp: Backport of commit #432250.
	  (Fix the GUI < > bug on sending)

2005-07-09 04:25 +0000 [r432926]  mattr

	* kopete/protocols/oscar/icq/icqcontact.cpp,
	  kopete/protocols/oscar/oscaraccount.cpp,
	  kopete/protocols/oscar/liboscar/messagereceivertask.cpp,
	  kopete/protocols/oscar/liboscar/oscartypeclasses.h: Backport the
	  fixes for bugs 103300 and 103249 Should be in KDE 3.4.2 (Kopete
	  0.10.3) CCBUGS: 103300, 103249

2005-07-14 08:52 +0000 [r434468]  ogoffart

	* kopete/protocols/msn/msnswitchboardsocket.cpp: Backport: Fix
	  custom emoticon in html tag

2005-07-15 16:02 +0000 [r434839]  swinter

	* wifi/kcmwifi/wificonfig.cpp: bug fix for Ad-Hoc networks
	  BUG:109130

2005-07-16 12:03 +0000 [r435254]  ogoffart

	* kopete/kopete/chatwindow/chatview.cpp: Backport Fix Bug 104621:
	  Raise window doesn't work if the chat window is minimized

2005-07-16 12:15 +0000 [r435258]  ogoffart

	* kopete/kopete/chatwindow/chatmemberslistwidget.cpp: Backport FIX
	  Bug 107046: still able to open a chat to yourself in the chat
	  window members list CCBUG: 107046

2005-07-16 17:49 +0000 [r435358]  mlarouche

	* kopete/protocols/msn/msnnotifysocket.cpp,
	  kopete/protocols/msn/msnsecureloginhandler.h (added),
	  kopete/protocols/msn/msnnotifysocket.h,
	  kopete/protocols/msn/Makefile.am,
	  kopete/protocols/msn/sslloginhandler.cpp (removed),
	  kopete/protocols/msn/msnsecureloginhandler.cpp (added),
	  kopete/protocols/msn/sslloginhandler.h (removed): Backport the
	  MSNSecureLoginHandler class. Should fix crash related to OpenSSL
	  and lag when login.

2005-07-16 20:16 +0000 [r435395]  mattr

	* kopete/protocols/oscar/icq/icqcontact.cpp: backport the fix for
	  bug 105786. It should be in KDE 3.4.2 (Kopete 0.10.3) CCBUG:
	  105786

2005-07-16 22:03 +0000 [r435423]  porten

	* kppp/pppstats.cpp, kppp/ChangeLog: backported Ivan's interface
	  check fix

2005-07-17 16:10 +0000 [r435607]  ogoffart

	* kopete/protocols/msn/msnsecureloginhandler.cpp: Backport: don't
	  output the password in the debug

2005-07-17 19:56 +0000 [r435664]  ogoffart

	* kopete/libkopete/kopetexsl.cpp: Backport xsltFree fix

2005-07-19 12:47 +0000 [r436268]  wstephens

	* kopete/protocols/groupwise/gwaccount.cpp: Backport fix for adding
	  contacts to your list who have messaged you.

2005-07-19 15:34 +0000 [r436359]  gj

	* kopete/protocols/gadu/libgadu/events.c,
	  kopete/protocols/gadu/libgadu/pubdir.c,
	  kopete/protocols/gadu/libgadu/libgadu.c,
	  kopete/protocols/gadu/libgadu/common.c,
	  kopete/protocols/gadu/libgadu/compat.h,
	  kopete/protocols/gadu/libgadu/dcc.c,
	  kopete/protocols/gadu/libgadu/pubdir50.c,
	  kopete/protocols/gadu/libgadu/libgadu.h,
	  kopete/protocols/gadu/libgadu/http.c: upgrade to 1.6b3, fixing
	  all SECURITY issues

2005-07-20 11:32 +0000 [r436851]  coolo

	* kopete/protocols/groupwise/ui/gwcontactpropswidget.ui: fixuifiles

