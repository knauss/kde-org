---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE Ships Applications 19.08.2.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE Ships Applications 19.08.2
version: 19.08.2
---

{{% i18n_date %}}

{{% i18n_var "Today KDE released the second stability update for <a href='%[1]s'>KDE Applications %[2]s</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../19.08.0" "19.08" %}}

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize, Spectacle, among others.

Improvements include:

- High-DPI support got improved in Konsole and other applications
- Switching between different searches in Dolphin now correctly updates search parameters
- KMail can again save messages directly to remote folders
