---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE Ships Applications 19.04.
layout: application
release: applications-19.04.0
title: KDE Ships KDE Applications 19.04.0
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---

{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

{{% i18n_var "The KDE community is happy to announce the release of KDE Applications %[1]s." "19.04" %}}

{{% i18n_var "Our community continuously works on improving the software included in our KDE Application series. Along with new features, we improve the design, usability and stability of all our utilities, games, and creativity tools. Our aim is to make your life easier by making KDE software more enjoyable to use. We hope you like all the new enhancements and bug fixes you'll find in %[1]s!" "19.04" %}}

## {{% i18n_var "What's new in KDE Applications %[1]s" "19.04" %}}

More than 150 bugs have been resolved. These fixes re-implement disabled features, normalize shortcuts, and solve crashes, making KDE Applications friendlier and allowing you to be more productive.


### File Management

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Dolphin</a> is KDE's file manager. It also connects to network services, such as SSH, FTP, and Samba servers, and comes with advanced tools to find and organize your data." "https://www.kde.org/applications/system/dolphin/" %}}

New features:


+ {{% i18n_var "We have expanded thumbnail support, so Dolphin can now display thumbnails for several new file types: <a href='%[1]s'>Microsoft Office</a> files, <a href='%[2]s'>.epub and .fb2 eBook</a> files, <a href='%[3]s'>Blender</a> files, and <a href='%[4]s'>PCX</a> files. Additionally, thumbnails for text files now show <a href='%[5]s'>syntax highlighting</a> for the text inside the thumbnail." "https://phabricator.kde.org/D18768" "https://phabricator.kde.org/D18738" "https://bugs.kde.org/show_bug.cgi?id=246050" "https://phabricator.kde.org/D19679" "https://phabricator.kde.org/D19432" %}} </li>

+ {{% i18n_var "You can now choose <a href='%[1]s'>which split view pane to close</a> when clicking the 'Close split' button." "https://bugs.kde.org/show_bug.cgi?id=312834"%}} </li>

+ {{% i18n_var "This version of Dolphin introduces <a href='%[1]s'>smarter tab placement</a>. When you open a folder in a new tab, the new tab will now be placed immediately to the right of the current one, instead of always at the end of the tab bar." "https://bugs.kde.org/show_bug.cgi?id=403690" %}} </li>

+ {{% i18n_var "<a href='%[1]s'>Tagging items</a> is now much more practical, as tags can be added or removed using the context menu." "https://phabricator.kde.org/D16872" %}} </li>

+ {{% i18n_var "We have <a href='%[1]s'>improved the default sorting</a> parameters for some commonly-used folders. By default, the Downloads folder is now sorted by date with grouping enabled, and the Recent Documents view (accessible by navigating to recentdocuments:/) is sorted by date with a list view selected." "https://phabricator.kde.org/D18697" %}} </li>

Bug fixes include:

+ {{% i18n_var "When using a modern version of the SMB protocol, you can now <a href='%[1]s'>discover Samba shares</a> for Mac and Linux machines." "https://phabricator.kde.org/D16299" %}} </li>

+ {{% i18n_var "<a href='%[1]s'>Re-arranging items in the Places panel</a> once again works properly when some items are hidden." "https://bugs.kde.org/show_bug.cgi?id=399430" %}} </li>

+ {{% i18n_var "After opening a new tab in Dolphin, that new tab's view now automatically gains <a href='%[1]s'>keyboard focus</a>." "https://bugs.kde.org/show_bug.cgi?id=401899" %}} </li>

+ {{% i18n_var "Dolphin now warns you if you try to quit while the <a href='%[1]s'>terminal panel is open</a> with a program running in it." "https://bugs.kde.org/show_bug.cgi?id=304816" %}} </li>
+ We fixed many memory leaks, improving Dolphin's overall performance.</li>

{{% i18n_var "The <a href='%[1]s'>AudioCD-KIO</a> allows other KDE applications to read audio from CDs and automatically convert it into other formats." "https://cgit.kde.org/audiocd-kio.git/" %}}


+ {{% i18n_var "The AudioCD-KIO now supports ripping into <a href='%[1]s'>Opus</a>." "https://bugs.kde.org/show_bug.cgi?id=313768" %}} </li>

+ {{% i18n_var "We made <a href='%[1]s'>CD info text</a> really transparent for viewing." "https://bugs.kde.org/show_bug.cgi?id=400849" %}} </li>

### Video Editing

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

{{% i18n_var "This is a landmark version for KDE's video editor. <a href='%[1]s'>Kdenlive</a> has gone through an extensive re-write of its core code as more than 60%% of its internals has changed, improving its overall architecture." "https://kde.org/applications/multimedia/kdenlive/" %}}

Improvements include:


+ The timeline has been rewritten to make use of QML.
+ When you put a clip on the timeline, audio and video always go to separate tracks.
+ The timeline now supports keyboard navigation: clips, compositions and keyframes can be moved with the keyboard. Also, the height of the tracks themselves is adjustable.
+ {{% i18n_var "In this version of Kdenlive, the in-track audio recording comes with a new <a href='%[1]s'>voice-over</a> feature." "https://bugs.kde.org/show_bug.cgi?id=367676" %}}
+ We have improved copy/paste: it works between different project windows. The proxy clip management has also been enhanced, as clips can now be individually deleted.
+ Version 19.04 sees the return of support for external BlackMagic monitor displays and there are also new on-monitor preset guides.
+ We have improved the keyframe handling, giving it a more consistent look and workflow. The titler has also been improved by making the align buttons snap to safe zones, adding configurable guides and background colors, and displaying missing items.
+ We fixed the timeline corruption bug that misplaced or missed clips which was triggered when you moved a group of clips.
+ We fixed the JPG image bug that rendered images as white screens on Windows. We also fixed the bugs affecting screen capture on Windows.
+ Apart from all of the above, we have added many small usability enhancements that will make using Kdenlive easier and smoother.

### Office

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Okular</a> is KDE's multipurpose document viewer. Ideal for reading and annotating PDFs, it can also open ODF files (as used by LibreOffice and OpenOffice), ebooks published as ePub files, most common comic book files, PostScript files, and many more." "https://kde.org/applications/graphics/okular/" %}}

Improvements include:


+ {{% i18n_var "To help you ensure your documents are always a perfect fit, we've added <a href='%[1]s'>scaling options</a> to Okular's Print dialog." "https://bugs.kde.org/show_bug.cgi?id=348172" %}}
+ {{% i18n_var "Okular now supports viewing and verifying <a href='%[1]s'>digital signatures</a> in PDF files." "https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e" %}}
+ {{% i18n_var "Thanks to improved cross-application integration, Okular now supports editing LaTeX documents in <a href='%[1]s'>TexStudio</a>." "https://bugs.kde.org/show_bug.cgi?id=404120" %}}
+ {{% i18n_var "Improved support for <a href='%[1]s'>touchscreen navigation</a> means you will be able to move backwards and forwards using a touchscreen when in Presentation mode." "https://phabricator.kde.org/D18118" %}}
+ {{% i18n_var "Users who prefer manipulating documents from the command-line will be able to perform smart <a href='%[1]s'>text search</a> with the new command-line flag that lets you open a document and highlight all occurrences of a given piece of text." "https://bugs.kde.org/show_bug.cgi?id=362038" %}}
+ {{% i18n_var "Okular now properly displays links in <a href='%[1]s'>Markdown documents</a> that span more than one line." "https://bugs.kde.org/show_bug.cgi?id=403247" %}}
+ {{% i18n_var "The <a href='%[1]s'>trim tools</a> have fancy new icons." "https://bugs.kde.org/show_bug.cgi?id=397768" %}}

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>KMail</a> is KDE's privacy-protecting email client. Part of the <a href='%[2]s'>Kontact groupware suite</a>, KMail supports all email systems and allows you to organize your messages into a shared virtual inbox or into separate accounts (your choice). It supports all kinds of message encryption and signing, and lets you share data such as contacts, meeting dates, and travel information with other Kontact applications." "https://kde.org/applications/internet/kmail/" "https://kde.org/applications/office/kontact/" %}}

Improvements include:


+ Move over, Grammarly! This version of KMail comes with support for languagetools (grammar checker) and grammalecte (French-only grammar checker).
+ {{% i18n_var "Phone numbers in emails are now detected and can be dialed directly via <a href='%[1]s'>KDE Connect</a>." "https://community.kde.org/KDEConnect" %}}
+ {{% i18n_var "KMail now has an option to <a href='%[1]s'>start directly in system tray</a> without opening the main window." "https://phabricator.kde.org/D19189" %}}
+ We have improved the Markdown plugin support.
+ Fetching mails via IMAP no longer gets stuck when login fails.
+ We also made numerous fixes in KMail's backend Akonadi to improve reliability and performance.

{{% i18n_var "<a href='%[1]s'>KOrganizer</a> is Kontact's calendar component, managing all your events." "https://kde.org/applications/office/korganizer/" %}}

+ {{% i18n_var "Recurrent events from <a href='%[1]s'>Google Calendar</a> are again synchronized correctly." "https://bugs.kde.org/show_bug.cgi?id=334569" %}}
+ {{% i18n_var "The event reminder window now remembers to <a href='%[1]s'>show on all desktops</a>." "https://phabricator.kde.org/D16247" %}}
+ {{% i18n_var "We modernized the look of the <a href='%[1]s'>event views</a>." "https://phabricator.kde.org/T9420" %}}

{{% i18n_var "<a href='%[1]s'>Kitinerary</a> is Kontact's brand new travel assistant that will help you get to your location and advise you on your way." "https://cgit.kde.org/kitinerary.git/" %}}

- There is a new generic extractor for RCT2 tickets (e.g. used by railway companies such as DSB, ÖBB, SBB, NS).
- Airport name detection and disambiguation have been greatly improved.
- We added new custom extractors for previously unsupported providers (e.g. BCD Travel, NH Group), and improved format/language variations of already supported providers (e.g. SNCF, Easyjet, Booking.com, Hertz).

### Development

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Kate</a> is KDE's full-featured text editor, ideal for programming thanks to features such as tabs, split-view mode, syntax highlighting, a built-in terminal panel, word completion, regular expressions search and substitution, and many more via the flexible plugin infrastructure." "https://kde.org/applications/utilities/kate/" %}}

Improvements include:

- Kate can now show all invisible whitespace characters, not just some.
- You can easily enable and disable Static Word Wrap by using its own menu entry on a per-document basis, without having to change the global default setting.
- The file and tab context menus now include a bunch of useful new actions, such as Rename, Delete, Open Containing Folder, Copy File Path, Compare [with another open file], and Properties.
- This version of Kate ships with more plugins enabled by default, including the popular and useful inline Terminal feature.
- When quitting, Kate no longer prompts you to acknowledge files that were modified on the disk by some other process, such as a source control change.
- The plugin’s tree view now correctly displays all menu items for git entries that have umlauts in their names.
- When opening multiple files using the command line, the files are opened in new tabs in the same order as specified in the command line.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Konsole</a> is KDE's terminal emulator. It supports tabs, translucent backgrounds, split-view mode, customizable color schemes and keyboard shortcuts, directory and SSH bookmarking, and many other features." "https://kde.org/applications/system/konsole/" %}}

Improvements include:


+ Tab management has seen a number of improvements that will help you be more productive. New tabs can be created by middle-clicking on empty parts of the tab bar, and there's also an option that allows you to close tabs by middle-clicking on them. Close buttons are displayed on tabs by default, and icons will be displayed only when using a profile with a custom icon. Last but not least, the Ctrl+Tab shortcut allows you to quickly switch between the current and previous tab.
+ {{% i18n_var "The Edit Profile dialog received a huge <a href='%[1]s'>user interface overhaul</a>." "https://phabricator.kde.org/D17244" %}}
+ The Breeze color scheme is now used as the default Konsole color scheme, and we have improved its contrast and consistency with the system-wide Breeze theme.
+ We have resolved the issues when displaying bold text.
+ Konsole now correctly displays the underline-style cursor.
+ {{% i18n_var "We have improved the display of <a href='%[1]s'>box and line characters</a>, as well as of <a href='%[2]s'>Emoji characters</a>." "https://bugs.kde.org/show_bug.cgi?id=402415" "https://bugs.kde.org/show_bug.cgi?id=401298" %}}
+ Profile switching shortcuts now switch the current tab’s profile instead of opening a new tab with the other profile.
+ Inactive tabs that receive a notification and have their title text color changed now again reset the color to the normal tab title text color when the tab is activated.
+ The 'Vary the background for each tab' feature now works when the base background color is very dark or black.

{{% i18n_var "<a href='%[1]s'>Lokalize</a> is a computer-aided translation system that focuses on productivity and quality assurance. It is targeted at software translation, but also integrates external conversion tools for translating office documents." "https://kde.org/applications/development/lokalize/" %}}

Improvements include:

- Lokalize now supports viewing the translation source with a custom editor.
- We improved the DockWidgets location, and the way settings are saved and restored.
- The position in .po files is now preserved when filtering messages.
- We fixed a number of UI bugs (developer comments, RegExp toggle in mass replace, fuzzy empty message count, …).

### Utilities

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Gwenview</a> is an advanced image viewer and organizer with intuitive and easy-to-use editing tools." "https://kde.org/applications/graphics/gwenview/" %}}

Improvements include:


+ {{% i18n_var "The version of Gwenview that ships with Applications 19.04 includes full <a href='%[1]s'>touchscreen support</a>, with gestures for swiping, zooming, panning, and more." "https://phabricator.kde.org/D13901" %}}
+ {{% i18n_var "Another enhancement added to Gwenview is full <a href='%[1]s'>High DPI support</a>, which will make images look great on high-resolution screens." "https://bugs.kde.org/show_bug.cgi?id=373178" %}}
+ {{% i18n_var "Improved support for <a href='%[1]s'>back and forward mouse buttons</a> allows you to navigate between images by pressing those buttons." "https://phabricator.kde.org/D14583" %}}
+ {{% i18n_var "You can now use Gwenview to open image files created with <a href='%[1]s'>Krita</a> – everyone’s favorite digital painting tool." "https://krita.org/" %}}
+ {{% i18n_var "Gwenview now supports large <a href='%[1]s'>512 px thumbnails</a>, allowing you to preview your images more easily." "https://phabricator.kde.org/D6083" %}}
+ {{% i18n_var "Gwenview now uses the <a href='%[1]s'>standard Ctrl+L keyboard shortcut</a> to move focus to the URL field." "https://bugs.kde.org/show_bug.cgi?id=395184" %}}
+ {{% i18n_var "You can now use the <a href='%[1]s'>Filter-by-name feature</a> with the Ctrl+I shortcut, just like in Dolphin." "https://bugs.kde.org/show_bug.cgi?id=386531" %}}

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Spectacle</a> is Plasma's screenshot application. You can grab full desktops spanning several screens, individual screens, windows, sections of windows, or custom regions using the rectangle selection feature." "https://kde.org/applications/graphics/spectacle/" %}}

Improvements include:


+ {{% i18n_var "We have extended the Rectangular Region mode with a few new options. It can be configured to <a href='%[1]s'>auto-accept</a> the dragged box instead of asking you to adjust it first. There is also a new default option to remember the current <a href='%[2]s'>rectangular region</a> selection box, but only until the program is closed." "https://bugs.kde.org/show_bug.cgi?id=404829" "https://phabricator.kde.org/D19117" %}}
+ {{% i18n_var "You can configure what happens when the screenshot shortcut is pressed <a href='%[1]s'>while Spectacle is already running</a>." "https://phabricator.kde.org/T9855" %}}
+ {{% i18n_var "Spectacle allows you to change the <a href='%[1]s'>compression level</a> for lossy image formats." "https://bugs.kde.org/show_bug.cgi?id=63151" %}}
+ {{% i18n_var "Save settings shows you what the <a href='%[1]s'>filename of a screenshot</a> will look like. You can also easily tweak the <a href='%[2]s'>filename template</a> to your preferences by simply clicking on placeholders." "https://bugs.kde.org/show_bug.cgi?id=381175" "https://bugs.kde.org/show_bug.cgi?id=390856" %}}
+ Spectacle no longer displays both “Full Screen (All Monitors)” and “Current Screen” options when your computer only has one screen.
+ The help text in Rectangular Region mode now shows up in the middle of the primary display, rather than split between the screens.
+ When run on Wayland, Spectacle only includes the features that work.

### Games and Education

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

{{% i18n_var "Our application series includes numerous <a href='%[1]s'>games</a> and <a href='%[2]s'>educational applications</a>." "https://games.kde.org/" "https://edu.kde.org/" %}}

{{% i18n_var "<a href='%[1]s'>KmPlot</a> is a mathematical function plotter. It has a powerful built-in parser. The graphs can be colorized and the view is scalable, allowing you to zoom to the level you need. Users can plot different functions simultaneously and combine them to build new functions." "https://kde.org/applications/education/kmplot" %}}


+ {{% i18n_var "You can now zoom in by holding down Ctrl and using the <a href='%[1]s'>mouse wheel</a>." "https://bugs.kde.org/show_bug.cgi?id=159772" %}}
+ {{% i18n_var "This version of Kmplot introduces the <a href='%[1]s'>print preview</a> option." "https://phabricator.kde.org/D17626" %}}

+ {{% i18n_var "Root value or (x,y) pair can now be copied to <a href='%[1]s'>clipboard</a>." "https://bugs.kde.org/show_bug.cgi?id=308168%" %}}

{{% i18n_var "<a href='%[1]s'>Kolf</a> is a miniature golf game." "https://kde.org/applications/games/kolf/" %}}

+ {{% i18n_var "We have restored <a href='%[1]s'>sound support</a>." "https://phabricator.kde.org/D16978" %}}

+ Kolf has been successfully ported away from kdelibs4.
