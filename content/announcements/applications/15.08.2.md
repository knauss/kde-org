---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE Ships KDE Applications 15.08.2
layout: application
title: KDE Ships KDE Applications 15.08.2
version: 15.08.2
---

{{% i18n_var "October 13, 2015. Today KDE released the second stability update for <a href='%[1]s'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../15.08.0" %}}

More than 30 recorded bugfixes include improvements to ark, gwenview, kate, kbruch, kdelibs, kdepim, lokalize and umbrello.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.13" %}}