---
aliases:
- ../4.6
date: '2011-01-26'
custom_about: true
custom_contact: true
title: KDE надає вам нові можливості керування робочим простором, нові програми та
  нову платформу розробки
---

<p>
KDE із задоволенням повідомляє про свій найсвіжіший набір випусків, зокрема оновлену версію робочого простору Плазми KDE, програм KDE та платформи розробки KDE. Ці випуски, які позначено номером версії 4.6, надають у розпорядження користувачів багато нових можливостей у кожній з трьох лінійок продуктів KDE. Серед цих нових можливостей:
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w09.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w09.png" class="img-fluid" alt="KDE Plasma Desktop, Gwenview and KRunner in 4.6">
	</a> <br/>
	<em>KDE Plasma Desktop, Gwenview and KRunner in 4.6</em>
</div>
<br/>

<h3>
<a href="./plasma">
Керуйте вашим робочим простором Плазми
</a>
</h3>

<p>
<a href="./plasma">
<img src="/announcements/4/4.6.0/images/plasma.png" class="app-icon float-left m-3" alt="Робочі простори Плазми KDE 4.6.0" />
</a>

<b>Робочі простори Плазми KDE</b> нової версії отримали нові можливості завдяки новій системі просторів дій, що спрощує прив’язку програм до певних просторів дій, призначених зокрема для виконання робочих чи розважальних завдань. Нова система керування живленням має спрощений інтерфейс налаштування і ще ширші можливості. KWin, програмою для керування вікнами у робочому просторі Плазми тепер можна керувати за допомогою скриптів, покращено також візуальні елементи стільниці. <b>Плазма для субноутбуків</b>, інтерфейс оптимізований для використання на мобільних пристроях, тепер значно швидший та простіший у користуванні на пристроях з сенсорним інтерфейсом. Докладніше про це можна дізнатися з <a href="./plasma">Оголошення про випуск робочого простору KDE 4.6</a>.

</p>

<h3>
<a href="./applications">
Контекстний перегляд у Dolphin
</a>
</h3>

<p>

<a href="./applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="Програми KDE 4.6.0"/>
</a>
</a>
Багатьма <b>командами розробників програм KDE</b> також було випущено нові версії. Зокрема слід звернути увагу на можливості з побудови маршрутів у віртуальному глобусі KDE, Marble та додаткові можливості з фільтрування та пошуку за метаданими файлів у програмі для керування файлами KDE, Dolphin, — контекстний перегляд. Значно покращено програми збірки ігор KDE, програма для перегляду зображень Gwenview та програма для створення знімків вікон KSnapshot нових версій здатні оприлюднювати зображення на декількох популярних сайтах соціальних мереж. Докладніше про це можна дізнатися з <a href="./applications">Оголошення щодо програм KDE 4.6</a>.<br /><br />
</p>

<h3>
<a href="./platform">
Мобільний профіль надає змогу значно полегшити платформу KDE
</a>
</h3>

<p>

<a href="./platform">
<img src="/announcements/4/4.6.0/images/platform.png" class="app-icon float-left m-3" alt="Платформа розробки KDE 4.6.0"/>
</a>

<b>Платформа KDE</b>, на основі якої побудовано робочий простір Плазми та програми KDE, у новій версії надає нові можливості всім програмам KDE. Новий профіль збирання для мобільних пристроїв спрощує збирання програм для портативних пристроїв. Оболонка Плазми тепер підтримує створення віджетів на основі QML, декларативної мови Qt, на надає розробникам нові інтерфейси Javascript для взаємодії з даними. Nepomuk, технологія, що забезпечує роботу з метаданими та можливості з семантичного пошуку у програмах KDE, тепер має графічний інтерфейс для створення резервних копій даних та відновлення даних з резервних копій. Замість застарілого інтерфейсу HAL для роботи з обладнанням можна використовувати UPower, UDev і UDisks. Значно покращено підтримку Bluetooth. Покращено набір віджетів та стилів Oxygen. За допомогою нової теми Oxygen для програм GTK користувачі зможуть отримати однорідний вигляд програм робочого простору Плазми. Докладніше про це можна дізнатися з <a href="./platform">Оголошення про випуск платформи KDE 4.6</a>.

</p>

<h4>
    Розкажіть іншим і будьте свідком результатів: мітка «KDE»
</h4>
<p align="justify">
Спільнота KDE буде вдячна вам за <strong>поширення інформації</strong> у соціальних мережах. Надсилайте статті на сайти новин, використовуйте канали обміну повідомленнями, зокрема delicious, digg, reddit, twitter, identi.ca. Вивантажуйте знімки вікон на служби зберігання зображень, зокрема Facebook, FlickR, ipernity і Picasa, і надсилайте їх до відповідних груп. Створюйте відеосюжети і вивантажуйте їх на YouTube, Blip.tv, Vimeo та інші служби. Не забудьте позначити вивантажений матеріал <em>міткою <strong>kde</strong></em>, щоб зацікавленим особам було легше його знайти, а також щоб полегшити команді KDE створення звітів щодо поширення оголошення про випуск KDE SC 4.6
. <strong>Допоможіть нам поширити інформацію, станьте учасником цієї події!</strong></p>

<p align="justify">
Ви можете спостерігати за подіями навколо випуску 4.6 у режимі реального часу у соціальній мережі за допомогою
<a href="http://buzz.kde.org"><strong>подачі новин Спільноти KDE</strong></a>. На цьому сайті будуть збиратися всі записи з identi.ca, twitter, youtube, flickr, picasaweb, блогів та багатьох інших сайтів соціальних мереж у режимі реального часу. Подачу новин можна знайти на <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/news/technology/kde_software_compilation_4_6_0_released"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/f9d9t/kde_software_compilation_460_released/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde46"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde46"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde46"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde46"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde46"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">мікрокнопки</a></span>
</div>

<h4>Підтримка KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.6.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify">За допомогою нової <a
href="http://jointhegame.kde.org/">програми підтримки членством</a> ви за 25&euro; на квартал можете підтримати міжнародну спільноту KDE у продовженні розробки вільного програмного забезпечення найкращої якості.</p>

<p>&nbsp;</p>
