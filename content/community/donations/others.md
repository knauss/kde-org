---
title: "Other ways to Donate"
hidden: true
---

### Donate through Paypal

<p>
This is the preferred way to <a href="../">donate</a> to KDE.
</p>

### Donate through Money Transfer

<p>
You may remit your donation to the account below. Be aware of applicable fees. Since July 2003 new EU regulations apply for cross-border transfers in euros and the fees charged by banks for such transactions may no longer exceed the fees that apply to domestic transfers. Please read the <a href="http://europa.eu/rapid/pressReleasesAction.do?reference=MEMO/03/140">FAQ on cross-border money transfers within the EU</a> to make sure that you do not incur unnecessary fees. Contact your bank for details and the fees that apply to you.
</p>
<pre>
K Desktop Environment e.V.
Account-Nr. 0 66 64 46
BLZ 200 700 24
Deutsche Bank Privat und Gesch&auml;ftskunden
Hamburg, Germany

IBAN : DE82 2007 0024 0066 6446 00
SWIFT-BIC: DEUTDEDBHAM

</pre>

### Donate through Personal Check

<p>You can send regular US checks to the following address:
</p>
<pre>
K Desktop Environment e.V.
c/o Celeste Paul
1897 Oracle Way
Apt #707
Reston, VA 20190
</pre>
<p>
Use "KDE e.V. - Celeste Lyn Paul" in the "Pay to the order of..." line.
</p>

### Benevity

<p>Some companies offer donation matching though Benevity.</p>
<p>If yours does you can select KDE e.V. at <a href="https://causes.benevity.org/causes/276-2767060345">https://causes.benevity.org/causes/276-2767060345</a>.</p>

### Amazon Smile

<p>If you are an Amazon.de user you can use <a href="https://smile.amazon.de/">Amazon Smile</a>, where Amazon donates 0.5% of the price of your eligible purchases to KDE e.V.</p>
<p>You can select KDE e.V. at <a href="https://smile.amazon.de/gp/chpf/homepage/ref=smi_se_scyc_srch_stsr?q=kde+ev">https://smile.amazon.de/gp/chpf/homepage/ref=smi_se_scyc_srch_stsr?q=kde+ev</a></p>

<hr style="margin-top: 2em; margin-bottom: 0.5em; border-style: inset; border-width: 1px;">

<p>
Please contact the <a href="ma&#x69;&#x6c;&#116;&#x6f;:k&#100;&#101;-ev&#00045;&#116;r&#101;asu&#x72;er&#064;&#x6b;d&#x65;&#46;o&#114;&#103;">KDE e.V. treasurer</a> for assistance or for any questions that you might have. Your contribution is very much appreciated! 
</p>
